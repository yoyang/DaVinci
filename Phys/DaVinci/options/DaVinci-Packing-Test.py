###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
########################################################################
#
# $Id: DaVinci.py,v 1.45 2010-03-17 17:18:28 pkoppenb Exp $
#
# Options for a typical DaVinci job
#
# @author Patrick Koppenburg
# @date 2008-08-06
#
########################################################################
from Gaudi.Configuration import *
from Configurables import GaudiSequencer
from Configurables import DaVinci, PhysConf
########################################################################

################################################################
#
# Standard configuration
#
DaVinci().EvtMax =  1000                   # Number of events
DaVinci().PrintFreq = 100
#DaVinci().SkipEvents = 200                       # Events to skip
DaVinci().DataType = "2012"                    # Must be given
DaVinci().Simulation   = False
DaVinci().HistogramFile = "packing.root"    # Histogram file
DaVinci().TupleFile = "DVNtuples.root"          # Ntuple
DaVinci().InputType = "MDST"

MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

########################################################################

#from Configurables import ConversionDODMapper
#mapper = ConversionDODMapper( OutputLevel = 3 )

#from Configurables import ParticlesAndVerticesMapper
#pvmapper = ParticlesAndVerticesMapper( OutputLevel = 3 )

# list of (<dest path regexp>, <source>)
#mapper.Transformations = [
    #('(.*)/pRec/Track/Best',     '$1/Rec/Track/Best'), # packing
    #('(.*)/Rec/Track/Best',      '$1/pRec/Track/Best') # unpacking
    #('(.*)/Phys/(.*)/Particles', '$1/pPhys/Particles'), # new Olivier's packing
#    ]

# note: if you want to pack/unpack (convert) everything under /Rec/, you can use:
#mapper.Transformations = [
    # Packing
    #('(.*)/pRec(.*)', '$1/Rec$2'),
#    ( '(.*)/Rec(.*)',  '$1/pRec$2'  ),
#    ( '(.*)/Phys(.*)', '$1/pPhys$2'  )
    #( '(.*)/Phys/(.*)/Particles', '$1/pPhys/Particles' )
#    ]

# algorithm types from source ClassIDs
#mapper.Algorithms[1550] = "UnpackTrack"
#mapper.Algorithms[1552] = "UnpackProtoParticle"
#mapper.Algorithms[1551] = "UnpackCaloHypo"
#mapper.Algorithms[1561] = "DataPacking::Unpack<LHCb::RichPIDPacker>"
#mapper.Algorithms[1571] = "DataPacking::Unpack<LHCb::MuonPIDPacker>"
#mapper.Algorithms[1553] = "UnpackRecVertex"
#mapper.Algorithms[1555] = "DataPacking::Unpack<LHCb::WeightsVectorPacker>"
#mapper.Algorithms[1581] = "UnpackParticlesAndVertices"
#mapper.Algorithms[1559] = "UnpackDecReport"

#mapper.Algorithms[000000] = "UnpackParticlesAndVertices"
# ... what are the ClassIDs of PackedTracks and PackedPariclesAndVertices?

# -- This is how we can control the OutputLevel of the algorithms we trigger
#for a in mapper.Algorithms.values():
#    mapper.AlgorithmsOutputLevels[a] = VERBOSE

## mapper.AlgorithmsOutputLevels["UnpackDecReport"] = 2
## mapper.AlgorithmsOutputLevels["UnpackTrack"] = 2
## mapper.AlgorithmsOutputLevels["UnpackParticlesAndVertices"] = 2

# Add the tool to DOD
#dod = DataOnDemandSvc()
#dod.NodeMappingTools = [pvmapper,mapper]
#dod.AlgMappingTools  = [pvmapper,mapper]
#dod.NodeMappingTools = [mapper,pvmapper]
#dod.AlgMappingTools  = [mapper,pvmapper]
# You may need to add:
#for path in ["Rec", "Rec/Track", "Phys", ...]:
#    dod.NodeMap[path] = "DataObject"

# Force the instantiation of DOD
#ApplicationMgr().ExtSvc += dod

# Check normal DoD still works
#from Configurables import ParticleEffPurMoni
## from StandardParticles import ( StdLooseANNElectrons, StdLooseElectrons,
##                                 StdTightANNElectrons, StdTightElectrons )
## le = ParticleEffPurMoni('ElectronPerf')
## le.Inputs = [ #"Phys/StdLooseANNElectrons/Particles"
##               "Phys/StdLooseElectrons/Particles"
##               #,"Phys/StdTightANNElectrons/Particles"
##               #,"Phys/StdTightElectrons/Particles"
##               ]
## DaVinci().UserAlgorithms += [ le ]

from Configurables import DataObjectVersionFilter
#from Configurables import UnpackParticlesAndVertices

# Test the automatic unpacking of data
DaVinci().UserAlgorithms += [
##    DataObjectVersionFilter( "CheckTracks",
##                              DataObjectLocation = "/Event/Bhadron/Rec/Track/Best",
##                              MaxVersion = 999 ),
##     DataObjectVersionFilter( "CheckDecReports",
##                              DataObjectLocation = "/Event/Strip/Phys/DecReports",
##                              MaxVersion = 999,
##                              OutputLevel = 3 ),
##     DataObjectVersionFilter( "CheckVeloClusters",
##                              DataObjectLocation = "/Event/Bhadron/Raw/Velo/Clusters",
##                              MaxVersion = 999,
##                              OutputLevel = 3 ),
    ## DataObjectVersionFilter( "CheckCaloElectrons",
    ##                          DataObjectLocation = "/Event/Bhadron/Rec/Calo/Electrons",
    ##                          MaxVersion = 999,
    ##                          OutputLevel = 3 ),
##     DataObjectVersionFilter( "CheckCaloPhotons",
##                              DataObjectLocation = "/Event/Bhadron/Rec/Calo/Photons",
##                              MaxVersion = 999,
##                             OutputLevel = 2 ),
##    DataObjectVersionFilter( "CheckLiteTTClusters",
##                             DataObjectLocation = "/Event/Bhadron/Raw/TT/LiteClusters",
##                             MaxVersion = 999,
##                             OutputLevel = 2 ),
##    DataObjectVersionFilter( "CheckLiteITClusters",
##                             DataObjectLocation = "/Event/Bhadron/Raw/IT/LiteClusters",
##                             MaxVersion = 999,
##                             OutputLevel = 2 ),
##    DataObjectVersionFilter( "CheckLiteVeloClusters",
##                             DataObjectLocation = "/Event/Bhadron/Raw/Velo/LiteClusters",
##                             MaxVersion = 999,
##                             OutputLevel = 2 ),
   #UnpackParticlesAndVertices("UnpackPsAndVs",
    #OutputLevel = 1,
    #                           OutputName="/Event/Bhadron/Phys/StdAllNoPIDsKaons/Particles"),
##     DataObjectVersionFilter( "CheckData1",
##                              DataObjectLocation = "/Event/Bhadron/Phys/StdAllNoPIDsKaons/Particles",
##                              MaxVersion = 999,
##                              OutputLevel = 3 ),
##     DataObjectVersionFilter( "CheckData2",
##                              DataObjectLocation = "/Event/Bhadron/Rec/Vertex/Primary",
##                              MaxVersion = 999,
##                              OutputLevel = 3 )
##     DataObjectVersionFilter( "CheckData3",
##                              DataObjectLocation = "/Event/Bhadron/Phys/StdAllNoPIDsPions/Particles",
##                              MaxVersion = 999,
##                              OutputLevel = 2 )
##         DataObjectVersionFilter( "CheckProtos",
##                              DataObjectLocation = "/Event/Bhadron/Rec/ProtoP/Charged",
##                              MaxVersion = 999,
##                              OutputLevel = 2 )
                DataObjectVersionFilter( "CheckRelInfo",
                             DataObjectLocation = "/Event/Leptonic/Phys/RareStrangeKPiPiPiDownLine/P2CVPion10_1",
                             MaxVersion = 999,
                             OutputLevel = 2 )
    ##     ParticleEffPurMoni( "CheckData4",
    ##                         Inputs = ["Phys/StdAllNoPIDsKaons/Particles"],
    ##                         #OutputLevel = 1,
    ##                         RootInTES = "/Event/Bhadron"
    ##                         )
##      DataObjectVersionFilter( "CheckMCPs",
##                               DataObjectLocation = "/Event/B2D0hD2KsPiPiDalitz/MC/Particles",
##                               MaxVersion = 999,
##                               OutputLevel = 2 ),
##     DataObjectVersionFilter( "CheckMCVs",
##                              DataObjectLocation = "/Event/B2D0hD2KsPiPiDalitz/MC/Vertices",
##                              MaxVersion = 999,
##                              OutputLevel = 2 )
    ]

## # Select the one with only one candidate
## from PhysConf.Filters import LoKi_Filters
## fltrs = LoKi_Filters( STRIP_Code = "HLT_PASS('StrippingB2D0PiD2KSHHDDBeauty2CharmLineDecision')" \
##                       " | HLT_PASS('StrippingB2D0KD2KSHHDDBeauty2CharmLineDecision')")
## from Configurables import DaVinci
## DaVinci().EventPreFilters = fltrs.filters ('Filters')

from GaudiConf import IOHelper
#IOHelper().inputFiles(['PFN:/r02/lhcb/jonesc/data/Collision12/LHCb/mDST/Reco14/Stripping20/BHADRON/MagDown/00020198_00001201_1.bhadron.mdst'])
IOHelper().inputFiles(['PFN:/usera/jonesc/NFS/data/tmp/00048279_00041826_1.leptonic.mdst'])

#IOHelper().inputFiles(['PFN:root://ccdcacli067.in2p3.fr:1094/pnfs/in2p3.fr/data/lhcb/buffer/lhcb/LHCb/Collision11/FULL.DST/00022719/0001/00022719_00014824_1.full.dst'])
