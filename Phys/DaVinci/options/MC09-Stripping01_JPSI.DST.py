###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#-- GAUDI jobOptions generated on Mon Nov  9 11:13:22 2009
#-- Contains event types : 
#--   30000000 - 4 files - 5256 events - 0.38 GBytes

from Gaudi.Configuration import * 

EventSelector().Input   = [
"   DATAFILE='PFN:castor:/castor/cern.ch/grid/lhcb/MC/MC09/JPSI.DST/00005438/0000/00005438_00000001_1.jpsi.dst' TYP='POOL_ROOTTREE' OPT='READ'",
"   DATAFILE='PFN:castor:/castor/cern.ch/grid/lhcb/MC/MC09/JPSI.DST/00005438/0000/00005438_00000002_1.jpsi.dst' TYP='POOL_ROOTTREE' OPT='READ'",
"   DATAFILE='PFN:castor:/castor/cern.ch/grid/lhcb/MC/MC09/JPSI.DST/00005438/0000/00005438_00000004_1.jpsi.dst' TYP='POOL_ROOTTREE' OPT='READ'",
"   DATAFILE='PFN:castor:/castor/cern.ch/grid/lhcb/MC/MC09/JPSI.DST/00005438/0000/00005438_00000005_1.jpsi.dst' TYP='POOL_ROOTTREE' OPT='READ'"]
