###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci, PhysConf, GaudiSequencer
########################################################################

DaVinci().EvtMax     = 1000
DaVinci().PrintFreq  = 1
#DaVinci().SkipEvents = 0

from Configurables import JsonConverter

json = JsonConverter("JsonConvert")
#json.OutputLevel = 1

# Use the following to only fill the json files for the given particles.
#json.EventDisplayParticles = "/Event/Phys/StdAllNoPIDsPions/Particles"

json.EventDisplayParticles = "/Event/Phys/StdTightMuons/Particles"
json.OutputDirectory = "jonesc-events-muons"

#json.EventDisplayParticles = "/Event/Phys/StdTightElectrons/Particles"
#json.OutputDirectory = "jonesc-events-electrons"

from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)

DaVinci().UserAlgorithms += [ json ]

from GaudiConf import IOHelper

#DaVinci().DataType   = "2012"
#IOHelper().inputFiles(['PFN:/r02/lhcb/jonesc/data/Collision12/LHCb/mDST/Reco14/Stripping20/BHADRON/MagDown/00020198_00001201_1.bhadron.mdst'])
#IOHelper().inputFiles(['PFN:/r02/lhcb/jonesc/data/Collision12/LHCb/DST/Reco14/Stripping20/BHADRONCOMPLETEEVENT/MagDown/00020738_00002980_1.bhadroncompleteevent.dst'])

#DaVinci().DataType   = "2015"
#IOHelper().inputFiles(['PFN:/r02/lhcb/jonesc/data/Collision15/LHCb/mDST/Reco15a/Stripping24/BHADRON/MagDown/00049592_00058961_1.bhadron.mdst'])
#IOHelper().inputFiles(['PFN:/var/pcjm/r02/lhcb/jonesc/data/Collision15/LHCb/DST/Reco15a/Stripping24/BHADRONCOMPLETEEVENT/MagDown/00049592_00059087_1.bhadroncompleteevent.dst'])

#importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco15a_Run164668.py")

DaVinci().DDDBtag   = "dddb-20140729"
DaVinci().CondDBtag = "sim-20140730-vc-md100"

DaVinci().DataType   = "2015"
DaVinci().Simulation = True
IOHelper('ROOT').inputFiles(['PFN:/var/nwork/pclc/jonesc/data/RunII/XDST/Brunel-1000ev.xdst'], clear=True)

FileCatalog().Catalogs = [ 'xmlcatalog_file:out.xml' ]
