###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
########################################################################
#
#  The simplest options file which reads some data and creates an ntuple.
#
#  Used for DaVinciTutorial 0 and 0.5
#
########################################################################


from Gaudi.Configuration import *
from Configurables import DaVinci

#take a Reco-14 Stripping-20 file for processing. Example DiMuon stream
DaVinci().Input = [ "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision12/DIMUON.DST/00020241/0000/00020241_00000272_1.dimuon.dst"]


#configure differently depending on which data you use
DaVinci().DataType="2012"
DaVinci().DDDBtag="dddb-20120831"
DaVinci().CondDBtag="cond-20120831"


#save an ntuple with the luminosity
DaVinci().Lumi=True
DaVinci().TupleFile="DVnTuples.root"

########################################################################
#
# If this doesn't work, then maybe the data doesn't exist any more
# or maybe there is something wrong with your environment/gangarc
#
########################################################################
