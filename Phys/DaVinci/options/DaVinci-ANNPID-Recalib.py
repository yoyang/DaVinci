###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

########################################################################
from Gaudi.Configuration import *
from Configurables import GaudiSequencer
from Configurables import DaVinci, PhysConf
########################################################################

DaVinci().EvtMax     = 10000
DaVinci().PrintFreq  = 1000
DaVinci().SkipEvents = 0        

from Configurables import DataObjectVersionFilter
DaVinci().UserAlgorithms += [
    DataObjectVersionFilter( "CheckRecSummary",
                             DataObjectLocation = "/Event/Rec/Summary",
                             MaxVersion = 999 ),
    DataObjectVersionFilter( "CheckProtos",
                             DataObjectLocation = "/Event/Rec/ProtoP/Charged",
                             MaxVersion = 999 ),
    DataObjectVersionFilter( "CheckTracks",
                             DataObjectLocation = "/Event/Rec/Track/Best",
                             MaxVersion = 999 ),
    DataObjectVersionFilter( "CheckRPIDs",
                             DataObjectLocation = "/Event/Rec/Rich/PIDs",
                             MaxVersion = 999 ),
    DataObjectVersionFilter( "CheckMPIDs",
                             DataObjectLocation = "/Event/Rec/Muon/MuonPID",
                             MaxVersion = 999 ),
    DataObjectVersionFilter( "CheckuDSTProtos",
                             DataObjectLocation = "/Event/Bhadron/Rec/ProtoP/Charged",
                             MaxVersion = 999 ),
    ]
 
#from Configurables import PhysConf
#PhysConf().AllowPIDRecalib = False

from Configurables import ChargedProtoANNPIDConf

#seq = GaudiSequencer("ANNPID")
#seq.RootInTES = "/Event/Bhadron"
#ChargedProtoANNPIDConf().DataType = DaVinci().DataType
#ChargedProtoANNPIDConf().NetworkVersions["2012"] = "MC12TuneV2"
#ChargedProtoANNPIDConf().RecoSequencer = seq
#ChargedProtoANNPIDConf().OutputLevel = 1

#ChargedProtoANNPIDConf().TrackTypes = ["Long"]
#ChargedProtoANNPIDConf().PIDTypes = ["Ghost"]
#ChargedProtoANNPIDConf().PIDTypes = ["Electron","Muon","Pion","Kaon","Proton","Ghost"]

#DaVinci().UserAlgorithms += [seq]

#from Configurables import ( ANNGlobalPID__ChargedProtoANNPIDTrainingTuple,
#                            ANNGlobalPID__ChargedProtoANNPIDTupleTool )
#pidtuple = ANNGlobalPID__ChargedProtoANNPIDTrainingTuple("ANNPIDTuple")
#pidtuple.addTool( ANNGlobalPID__ChargedProtoANNPIDTupleTool, name = "Tuple" )
#pidtuple.Tuple.NTupleLUN = "ANNPIDTUPLE"
#DaVinci().UserAlgorithms += [ pidtuple ]
#NTupleSvc().Output += ["ANNPIDTUPLE DATAFILE='ProtoPIDANN.tuples.root' TYP='ROOT' OPT='NEW'"]


from GaudiConf import IOHelper

#DaVinci().DataType   = "2012"
#IOHelper().inputFiles(['PFN:/r02/lhcb/jonesc/data/Collision12/LHCb/mDST/Reco14/Stripping20/BHADRON/MagDown/00020198_00001201_1.bhadron.mdst'])
#IOHelper().inputFiles(['PFN:/r02/lhcb/jonesc/data/Collision12/LHCb/DST/Reco14/Stripping20/BHADRONCOMPLETEEVENT/MagDown/00020738_00002980_1.bhadroncompleteevent.dst'])

DaVinci().DataType   = "2015"
#IOHelper().inputFiles(['PFN:/r02/lhcb/jonesc/data/Collision15/LHCb/mDST/Reco15a/Stripping24/BHADRON/MagDown/00049592_00058961_1.bhadron.mdst'])
IOHelper().inputFiles(['PFN:/var/pcjm/r02/lhcb/jonesc/data/Collision15/LHCb/DST/Reco15a/Stripping24/BHADRONCOMPLETEEVENT/MagDown/00049592_00059087_1.bhadroncompleteevent.dst'])

ApplicationMgr().ExtSvc += [ 'AuditorSvc' ]
from Configurables import AuditorSvc, FPEAuditor
AuditorSvc().Auditors += [ "FPEAuditor" ]
#AuditorSvc().Auditors += [ 'MemoryAuditor' ]
