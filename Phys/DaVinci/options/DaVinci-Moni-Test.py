###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

##############################################################################
from Configurables import DaVinci, LHCbApp
from Gaudi.Configuration import *
##############################################################################

importOptions("$APPCONFIGOPTS/DaVinci/DVMonitor-RealData.py")

#importOptions("$APPCONFIGOPTS/DaVinci/DataType-2010.py")

importOptions("$APPCONFIGOPTS/DaVinci/DataType-2011.py")

DaVinci().EvtMax = 1000

DaVinci().InputType = 'DST'

DaVinci().PrintFreq = 100

MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

LHCbApp().DDDBtag   = "head-20110722" 
LHCbApp().CondDBtag = "head-20110722"
