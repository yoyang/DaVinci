###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from subprocess import call
import os
execfile(os.path.expandvars("$TESLAROOT/tests/options/streams.py"))
exit_code = call(["python",os.path.expandvars("$TESLAROOT/tests/options/analyseInput.py")])
if exit_code:
    exit( exit_code )

for stream,lines in turbo_streams.iteritems():
    exit_code = call(["python",os.path.expandvars("$TESLAROOT/tests/options/analyseOutput.py"),stream])
    if exit_code:
        exit( exit_code )
