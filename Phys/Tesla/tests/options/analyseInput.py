###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import sys

import GaudiPython as GP
from GaudiConf import IOHelper
from Configurables import (
  LHCbApp,
  ApplicationMgr,
  DataOnDemandSvc,
  DecodeRawEvent,
  CondDB
)
from DAQSys.Decoders import DecoderDB
        

lhcbApp = LHCbApp()

# Pass file to open as first command line argument
# Needs to be same file as in TeslaStepStreams.py
from PRConfig import TestFileDB
TestFileDB.test_file_db['TeslaTest_2016raw_0x11361609_0x21361609'].run(configurable=lhcbApp)

LHCb = GP.gbl.LHCb
appConf = ApplicationMgr()

Hlt1SelReportsDecoder=DecoderDB["HltSelReportsDecoder/Hlt1SelReportsDecoder"].setup()
Hlt1DecReportsDecoder=DecoderDB["HltDecReportsDecoder/Hlt1DecReportsDecoder"].setup()
DataOnDemandSvc().AlgMap['Hlt1/SelReports'] = Hlt1SelReportsDecoder
DataOnDemandSvc().AlgMap['Hlt1/DecReports'] = Hlt1DecReportsDecoder
Hlt2SelReportsDecoder=DecoderDB["HltSelReportsDecoder/Hlt2SelReportsDecoder"].setup()
Hlt2DecReportsDecoder=DecoderDB["HltDecReportsDecoder/Hlt2DecReportsDecoder"].setup()
DataOnDemandSvc().AlgMap['Hlt2/SelReports'] = Hlt2SelReportsDecoder
DataOnDemandSvc().AlgMap['Hlt2/DecReports'] = Hlt2DecReportsDecoder

appConf.ExtSvc += [DataOnDemandSvc()]
appMgr = GP.AppMgr()

evt = appMgr.evtsvc()
import os
execfile(os.path.expandvars("$TESLAROOT/tests/options/streams.py"))

decsPerStream = {}
evtsPerStream = {}
for stream, lines in turbo_streams.iteritems():
    decsPerStream[stream] = 0
    evtsPerStream[stream] = 0

def routing_bits(rawevent):
    """Return a list with the 96 routing bit values."""
    rbbanks = rawevent.banks(LHCb.RawBank.HltRoutingBits)
    if rbbanks.size() < 1: return [0]*96  # some events don't have rb bank                                               
    assert rbbanks.size() == 1  # stop if we have multiple rb banks                                                     
    d = rbbanks[rbbanks.size() - 1].data()  # get last bank                                                             
    bits = "{:032b}{:032b}{:032b}".format(d[2], d[1], d[0])
    return map(int, reversed(bits))

# Number of events has to be the same as in TeslaStepsStreams.py
for i in range(0,500):
    appMgr.run(1)
    #print i
    decs = 0
    inStream = False
    offStream = False
    rawevent = evt['DAQ/RawEvent']
    rbits = routing_bits(rawevent)
    if rbits[95] != 1:
        continue
    if not evt["DAQ/ODIN"]:
        break
    if not evt["Hlt2/DecReports"]:
        print "ERROR: Hlt2 dec report missing"
        continue
    else:
        found = {}
        for stream, lines in turbo_streams.iteritems():
            found[stream] = False

        for name,dec in evt["Hlt2/DecReports"].decReports().iteritems():
            if name == 'Hlt2Global':
                continue
            if dec.decision()==1:
                for stream, lines in turbo_streams.iteritems():
                    if name[:-8] in lines["lines"]:
                        decsPerStream[stream] = decsPerStream[stream] + 1
                        found[stream] = True
                        
        for stream, lines in turbo_streams.iteritems():
            if found[stream]:
                evtsPerStream[stream] =  evtsPerStream[stream] + 1

print decsPerStream
print evtsPerStream
import pickle
pickle.dump( decsPerStream, open( "decsPerStream.p", "wb" ) )
pickle.dump( evtsPerStream, open( "evtsPerStream.p", "wb" ) )
