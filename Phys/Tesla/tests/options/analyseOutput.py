###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import sys

import GaudiPython as GP
from GaudiConf import IOHelper
from Configurables import (
  LHCbApp,
  ApplicationMgr,
  DataOnDemandSvc,
  DecodeRawEvent,
  HltDecReportsDecoder,
  CondDB
)
from DAQSys.Decoders import DecoderDB
        

lhcbApp = LHCbApp()

# Pass file to open as first command line argument
stream = sys.argv[-1]
prefix = ""
IOHelper('ROOT').inputFiles([prefix+stream+".dst"])

LHCb = GP.gbl.LHCb
appConf = ApplicationMgr()

Hlt1SelReportsDecoder=DecoderDB["HltSelReportsDecoder/Hlt1SelReportsDecoder"].setup()
Hlt1DecReportsDecoder=DecoderDB["HltDecReportsDecoder/Hlt1DecReportsDecoder"].setup()
DataOnDemandSvc().AlgMap['Hlt1/SelReports'] = Hlt1SelReportsDecoder
DataOnDemandSvc().AlgMap['Hlt1/DecReports'] = Hlt1DecReportsDecoder
Hlt2SelReportsDecoder=DecoderDB["HltSelReportsDecoder/Hlt2SelReportsDecoder"].setup()
Hlt2DecReportsDecoder=DecoderDB["HltDecReportsDecoder/Hlt2DecReportsDecoder"].setup()
DataOnDemandSvc().AlgMap['Hlt2/SelReports'] = Hlt2SelReportsDecoder
DataOnDemandSvc().AlgMap['Hlt2/DecReports'] = Hlt2DecReportsDecoder
TurboDecReportsDecoder=HltDecReportsDecoder("TurboDecReportsDecoder")
TurboDecReportsDecoder.OutputHltDecReportsLocation = 'Turbo/DecReports'+stream
turboReports = 'Turbo/DecReports'+stream
DataOnDemandSvc().AlgMap[turboReports] = TurboDecReportsDecoder


    
appConf.ExtSvc += [DataOnDemandSvc()]
appMgr = GP.AppMgr()

evt = appMgr.evtsvc()
import os
import sys
execfile(os.path.expandvars("$TESLAROOT/tests/options/streams.py"))

lines = turbo_streams[stream]["lines"]
lines = [line+"Decision" for line in lines]
print lines
decLocation = "Hlt2/DecReports"
if turbo_streams[stream]["prescale"] != None:
    print "Prescaled stream"
    decLocation = turboReports
    

allEvents = 0
streamDecs = 0
streamEvents = 0
for i in range(0,10000):
    appMgr.run(1)
    #print i
    decs = 0
    inStream = False
    offStream = False
    if not evt["DAQ/ODIN"]:
        break
    if not evt[decLocation]:
        print "ERROR: Hlt2 dec report missing. Location ", decLocation
        continue
    else:
        allEvents = allEvents + 1
        for name,dec in evt[decLocation].decReports().iteritems():
            if name == 'Hlt2Global':
                continue
            if dec.decision()==1:
                if name in lines:
                    inStream = True
                    decs = decs + 1
                    streamDecs = streamDecs + 1
                else:
                    offStream = True
        if inStream:
            streamEvents = streamEvents + 1

    assert inStream, "ERROR: Should not contain events without positive decision of stream"
    
    assert (inStream or not offStream), "ERROR: Should not contain events of other streams"


print "##### SUMMARY #####"
print "All Events ",allEvents, ", events in stream", streamEvents, ", postive line decisions ", streamDecs

if allEvents != streamEvents:
    print "ERROR: More events than should be in stream"

import pickle
decsPerStream = pickle.load( open( "decsPerStream.p", "rb" ) )
evtsPerStream = pickle.load( open( "evtsPerStream.p", "rb" ) )
if turbo_streams[stream]["prescale"] == None:
    if decsPerStream[stream] != streamDecs:
        print "ERROR : Number of input decision different to output"
        print "Should be ", decsPerStream[stream]
        assert False
    if evtsPerStream[stream] != streamEvents:
        print "ERROR : Number of input events different to output"
        print "Should be ", evntsPerStream[stream]
        assert False

else:
    print "INFO : Stream contains prescaled lines"
    print "Input decsions were ", decsPerStream[stream]
    print "Effective prescaling : ", float(streamDecs)/decsPerStream[stream]
    if streamDecs >= decsPerStream[stream] :
        print "ERROR : No prescaling applied"
        assert False
        
    print "Input events were ", evtsPerStream[stream]
    print "Effective prescaling : ", float(streamEvents)/evtsPerStream[stream]
    if streamEvents >= evtsPerStream[stream] :
        print "ERROR : No prescaling applied"
        assert False

