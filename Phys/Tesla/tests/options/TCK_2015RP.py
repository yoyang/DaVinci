###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import Tesla 
Tesla().Pack = True
Tesla().DataType = '2015'
Tesla().Simulation = False
Tesla().Mode = 'Offline'
Tesla().VertRepLoc = 'Hlt2'
Tesla().EvtMax=1000
Tesla().outputFile = "tesla_2015_TCK.dst"
Tesla().SplitRawEventInput = '0.4'
Tesla().SplitRawEventOutput = '0.4'

from Gaudi.Configuration import *
version='v10r0_0x00fa0051'
from TurboStreamProd.helpers import *
from TurboStreamProd import prodDict
lines = streamLines(prodDict,version,'DiMuon',debug=True)
lines += streamLines(prodDict,version,'Charm',debug=True)
lines += streamLines(prodDict,version,'CharmSpec',debug=True)

Tesla().TriggerLines = lines
Tesla().EnableLineChecker = False

from PRConfig.TestFileDB import test_file_db
input = test_file_db['TeslaTest_TCK_0x022600a2']
input.run(configurable=Tesla())
Tesla().KillInputTurbo = True
