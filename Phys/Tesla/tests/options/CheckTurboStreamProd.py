###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Script to check TCK-TurboStreamProd compatibility.

To be used in LHCbIntegrationTests and the TCK creation procedure.
Run with e.g.
    python $TESLAROOT/tests/options/CheckTurboStreamProd.py turbo_lines.json \
        '$APPCONFIGOPTS/Turbo/Streams_pp_2018_nocharmxsec.py'

"""
from __future__ import print_function
import argparse
import json
import re
import sys
from Gaudi.Configuration import importOptions
from Configurables import Tesla

parser = argparse.ArgumentParser(description="")
parser.add_argument("lines", help='JSON file with turbo lines (- for stdin).')
parser.add_argument("options", nargs='+', help="Appconfig options that configure Tesla")
args = parser.parse_args()

if args.lines == '-':
    turbo_lines = json.load(sys.stdin)
else:
    with open(args.lines) as f:
        turbo_lines = json.load(f)


for path in args.options:
    importOptions(path)


def ignored_line(line):
    return any(re.match(pattern, line) for pattern in Tesla().IgnoredLines)


checked_lines = [line for line in turbo_lines if not ignored_line(line)]
lines_in_streams = sum((v['lines'] for v in Tesla().Streams.values()), [])

orphan_lines = set(checked_lines).difference(set(lines_in_streams))
if orphan_lines:
    print("The following lines are missing from TurboStreamProd:\n"
          "{}".format(list(orphan_lines)), file=sys.stderr)
    sys.exit(1)
extra_lines = set(lines_in_streams).difference(set(checked_lines))
if extra_lines:
    print("Some lines are assigned to a stream but not present:\n"
          "{}".format(list(extra_lines)))
print("Passed")
