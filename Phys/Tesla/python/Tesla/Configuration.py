###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import logging as log
import os
import re

from Configurables import (
    AddressKillerAlg,
    AuditorSvc,
    CaloClusterCloner,
    CaloClusterMCTruth,
    CaloDigit2MCLinks2Table,
    CaloDigitCloner,
    CaloHypoCloner,
    ChargedPP2MC,
    CopyLinePersistenceLocations,
    CopyParticle2PVRelationsFromLinePersistenceLocations,
    CopyParticle2RelatedInfoFromLinePersistenceLocations,
    CopyProtoParticle2MCRelations,
    CopySignalMCParticles,
    DataOnDemandSvc,
    DataPacking__Pack_LHCb__CaloClusterPacker_ as PackCaloClusters,
    DataPacking__Unpack_LHCb__CaloClusterPacker_ as UnpackCaloClusters,
    DecodeRawEvent,
    DstConf,
    EventNodeKiller,
    EventSelector,
    Gaudi__DataCopy as DataCopy,
    Gaudi__DataLink as DataLink,
    GaudiSequencer,
    HltPackedDataDecoder,
    HltRoutingBitsFilter,
    HltSelReportsWriter,
    InputCopyStream,
    LHCbApp,
    LHCbConfigurableUser,
    LoKi__HDRFilter as HltDecReportsFilter,
    LoKiSvc,
    LumiAlgsConf,
    NeutralPP2MC,
    OutputStream,
    P2MCPFromProtoP,
    PackCaloHypo as PackCaloHypos,
    PackMCParticle,
    PackMCVertex,
    PackParticlesAndVertices,
    ProtoParticleCloner,
    PrintDuplicates,
    RawEventFormatConf,
    RawEventJuggler,
    RecSummaryFromSelReports,
    RecombineRawEvent,
    RecordStream,
    SequencerTimerTool,
    TCKLinePersistenceSvc,
    TESCheck,
    TeslaLineChecker,
    TeslaReportAlgo,
    TimingAuditor,
    TrackAssociator,
    TrackClonerWithClusters,
    TurboConf,
    TurboPrescaler,
    UnpackCaloHypo as UnpackCaloHypos,
    UnpackParticlesAndVertices,
    bankKiller
)
from Gaudi.Configuration import *
from GaudiConf import IOExtension, IOHelper
from GaudiConf import PersistRecoConf
import GaudiKernel.ProcessJobOptions
from LHCbKernel.Configuration import *
from RawEventCompat.Configuration import ReverseDict as RawEventLocationsToBanks


# Locations of Track and ProtoParticle objects created in HLT2 that are not
# defined as part of the 'standard reconstruction' that is saved in PersistReco
# We need to know these locations in Tesla so that we can truth match the objects
# See LHCBPS-1807 for more details, and how this list was obtained
_extra_track_locations = [
    # These two locations are defined as part of the standard HLT2 reconstruction
    # 'Hlt2/Track/Best/Downstream',
    # 'Hlt2/Track/Best/Long',
    'Hlt2/Track/Best/LongDownstreamClones',
    'Hlt2/Track/Best/LongV0Vertices',
    'Hlt2/Track/Best/Muon',
    'Hlt2/Track/Best/Ttrack',
    'Hlt2/Track/ComplementForward',
    'Hlt2/Track/Downstream',
    'Hlt2/Track/Forward',
    'Hlt2/Track/Match',
    'Hlt2/Track/Seeding',
    'Hlt2/TrackBest/PID/MuonSegments',
    'Hlt2/TrackEff/Track/FullDownstream',
    'Hlt2/TrackEff/Track/MuonTT',
    'Hlt2/TrackEff/Track/StandaloneMuon',
    'Hlt2/TrackEff/Track/VeloMuon'
]
_extra_protoparticle_locations = [
    # These two locations are defined as part of the standard HLT2 reconstruction
    # 'Hlt2/ProtoP/Downstream/Charged/WithAllPIDs',
    # 'Hlt2/ProtoP/Long/Charged/WithAllPIDs',
    'Hlt2/ProtoP/Best/FullDownstream',
    'Hlt2/ProtoP/Best/MuonTT',
    'Hlt2/ProtoP/Best/VeloMuon',
    'Hlt2/ProtoP/Hlt2/TrackEff/Velo'
]


def _strip_root(root, locs):
    """Return list of paths `loc` stripped of the `root` prefix.

    Examples
    --------

        >>> _strip_root('/Event/Turbo', ['/Event/Turbo/Foo'])
        'Foo'
        >>> _strip_root('/Event', ['/Event/Turbo/Foo'])
        'Turbo/Foo'
        >>> # Path is already relative
        >>> _strip_root('/Event', ['Turbo/Foo'])
        'Turbo/Foo'
        >>> # Complains if the prefix doesn't match the root
        >>> _strip_root('/Event/Ultra', ['/Event/Turbo/Foo'])
        AssertionError
    """
    root = root.rstrip('/')
    ret = []
    for l in locs:
        # Only need to modify paths that are already absolute
        if l.startswith('/'):
            # Strip off the root the prefixes the string
            l = l.replace(root + '/', '')
            assert not l.startswith('/'), \
                   'TES path still prefixed: {0}'.format(l)
        ret.append(l)
    return ret


class Tesla(LHCbConfigurableUser):
    __used_configurables__ = [ LHCbApp, LumiAlgsConf, RawEventJuggler, DecodeRawEvent, RawEventFormatConf, DstConf, RecombineRawEvent, PackParticlesAndVertices, TrackAssociator, ChargedPP2MC, TurboConf]

    __slots__ = {
            "EvtMax"		: -1    	# Maximum number of events to process
          , "SkipEvents"	: 0		# Skip to event
          , "Simulation"	: True 		# True implies use SimCond
          , "DataType"		: '2012' 	# Data type, can be [ 'DC06','2008' etc...]
          , "InputType"		: 'DST' 	# Input type, can be [ 'DST','LDST' etc...]
          , "DDDBtag" 		: 'default' 	# default as set in DDDBConf for DataType
          , "CondDBtag" 	: 'default' 	# default as set in DDDBConf for DataType
          , 'Persistency' 	: '' 		# None, Root or Pool?
          , 'OutputLevel' 	: 4 		# Er, output level
          , 'PackerOutputLevel' : 4             # Packer output level
          , 'DuplicateCheck' 	: False		# Do we want to add the algorithm to check for duplicates
          , 'KillInputTurbo' 	: False		# If rerunning Tesla, kill input Turbo locations
          , "outputFile" 	: 'Tesla.dst' 	# output filename
          , "outputPrefix"      : ''            # This is prepended to the stream name
          , "outputSuffix"      : '.dst'        # This is appended to the stream name
          , 'WriteFSR'    	: False 	# copy FSRs as required
          , 'PV'	        : "Offline"     # Associate to the PV chosen by the HLT or the offline one
          , 'VertRepLoc'	: "Hlt2"        # Do we use the maker from Hlt1 or Hlt2
          , 'TriggerLines'	: []            # Which trigger lines
          , 'Mode'	        : "Offline"     # "Online" (strip unnecessary banks and run lumi algorithms) or "Offline"?
                                            # "Online" means running on data from the pit
                                            # "Offline" is used on DSTs, i.e. the output of Brunel
          , 'Pack'	        : True          # Do we want to pack the objects?
          , 'SplitRawEventInput'	: 0.3
          , 'SplitRawEventOutput'	: 4.3
          , 'Monitors'	        : []            # Histogram monitors to run in the job
          , 'Histogram'  	: ""            # Name of histogram file
          , 'VetoJuggle'  	: True          # True if raw event removal not required in online mode
          , 'IgnoreDQFlags'  	: True          # Pass IgnoreDQFlags to LHCbApp
          , 'PersistRecoMC'  	: True          # Match persistReco objects to MC
          , 'ProtoTypesPR'  	: ["long","down"] # Which protoparticle types are in Turbo++
          , 'KillInputHlt2Reps'	: False         # Kill input Hlt2 reps to save space
          , 'InputHlt2RepsToKeep' : []          # List of selection names of reports to keep (if KillInputHlt2Reps is True)
          , 'HDRFilter'	        : False         # Filter on trigger lines or not
          , 'Park'	        : False         # If prescaled streams, make a parked version
          , 'PRtracks'  	: []            # PR track locations
          , 'PRchargedPP'  	: []            # PR protoparticle locations
          , 'PRclusters'  	: []            # PR cluster locations
          , 'PRneutralPP'  	: []            # PR neutral protoparticle locations
          , 'Streams'           : {}            # Streams, empty if no streaming
          , 'ValidateStreams'   : False         # Run validation to compare streams and TriggerLines
          , 'EnableLineChecker' : True          # Enable the comparison of lines in the DecReports and the input to TeslaReportsAlgo
          , 'IgnoredLines'      : [ ]   # Regexes for lines excluded from the comparison of DecReports and input to TeslaReportsAlgo
          , 'FilterMC' : False # Save a subset of the input MC particles and vertices under `Tesla.base, mimicking a microDST writer`
          , 'ILinePersistenceSvc' : "TCKLinePersistenceSvc"   # Implementation of the ILinePersistenceSvc to use (for >= 2017 data types)
          , 'CandidatesFromSelReports' : False  # If True and DataType >= 2017, take Turbo candidates from Hlt2SelReports rather than
                                                # from the DstData raw bank (i.e. behave as if DataType =< 2016)
          }
    _propertyDocDct ={
            "EvtMax"		: "Maximum number of events to process, default all"
            , "SkipEvents"	: "Events to skip, default     0"
            , "Simulation"	: "True implies use SimCond"
            , "DataType"	: "Data type, can be [ 'DC06','2008' ...]"
            , "InputType"	: "Input type, can be [ 'DST','LDST' ...]"
            , "DDDBtag" 	: "Databse tag, default as set in DDDBConf for DataType"
            , "CondDBtag" 	: "Databse tag, default as set in DDDBConf for DataType"
            , 'Persistency' 	: "Root or Pool?"
            , 'OutputLevel' 	: "Output level"
            , 'PackerOutputLevel': "Packer output level"
            , 'DuplicateCheck' 	: "Add the test for duplicates?"
            , 'KillInputTurbo' 	: "Are we rerunning Tesla"
            , "outputFile" 	: 'output filename, automatically selects MDF or InputCopyStream'
            , "outputPrefix"    : 'This is prepended to the stream name'
            , "outputSuffix"    : 'This is appended to the stream name'
            , 'WriteFSR'    	: 'copy FSRs as required'
            , 'PV'     	        : 'Associate to the PV chosen by the HLT or the offline one'
            , 'VertRepLoc'	: 'Do we use the maker from Hlt1 or Hlt2'
            , 'TriggerLines'    : 'Which trigger line to process'
            , 'Mode'     	: '"Online" (strip unnecessary banks and run lumi algorithms) or "Offline"?'
            , 'Pack'     	: 'Do we want to pack the object?'
            , 'SplitRawEventInput': "How is the event split up in the input? Propagated to RawEventJuggler() and DecodeRawEvent()."
            , 'SplitRawEventOutput': "How should the event be split up in the output?"
            , 'Monitors'        : 'Histogram monitors to run in the job'
            , 'Histogram'       : 'File name for histogram file'
            , 'VetoJuggle'      : 'Do we want to stop raw bank removal (assume happened further upstream)'
            , 'IgnoreDQFlags'  	: 'Pass IgnoreDQFlags to LHCbApp'
            , 'PersistRecoMC'  	: 'Match persistReco objects to MC'
            , 'ProtoTypesPR'  	: 'Which protoparticle types are in Turbo++'
            , 'KillInputHlt2Reps':'Kill input Hlt2 reps to save space'
            , 'InputHlt2RepsToKeep': 'List of selection names of reports to keep (if KillInputHlt2Reps is True)'
            , 'HDRFilter'	: 'Filter on trigger lines or not'
            , 'Park'	        : 'If prescaled streams, make a parked version'
            , 'PRtracks'  	: 'PR track locations'
            , 'PRchargedPP'  	: 'PR protoparticle locations'
            , 'PRclusters'  	: 'PR cluster locations'
            , 'PRneutralPP'  	: 'PR neutral protoparticle locations'
            , 'Streams'         : 'Streams, empty if no streaming'
            , 'ValidateStreams' : 'Run validation to compare streams and TriggerLines'
            , 'EnableLineChecker' : 'Enable the comparison of lines in the DecReports and the input to TeslaReportsAlgo'
            , 'IgnoredLines'      : 'Regexes for lines excluded from the comparison of DecReports and input to TeslaReportsAlgo'
            , 'FilterMC' : "Save a subset of the input MC particles and vertices under `Tesla.base`, mimicking a microDST writer"
            , 'ILinePersistenceSvc' : "Implementation of the ILinePersistenceSvc to use (for >= 2017 data types)"
            , 'CandidatesFromSelReports' : ("If True and DataType >= 2017, take Turbo candidates from Hlt2SelReports rather than "
                                            "from the DstData raw bank (i.e. behave as if DataType =< 2016)")

            }

    writerName = "DstWriter"
    teslaSeq = GaudiSequencer("TeslaSequence")
    base = "Turbo/"
    neutral_pp2mc_loc = "Relations/Turbo/NeutralPP2MC"


    def _safeSet(self,conf,param):
        for p in param:
            if (not self.isPropertySet(p)) or conf.isPropertySet(p):
                continue
            conf.setProp(p,self.getProp(p))

    def _configureDataOnDemand(self) :
        if 'DataOnDemandSvc' in ApplicationMgr().ExtSvc :
            ApplicationMgr().ExtSvc.pop('DataOnDemandSvc')
        else:
            dod = DataOnDemandSvc()
        if dod not in ApplicationMgr().ExtSvc :
            ApplicationMgr().ExtSvc.append( dod )

    def _configureHistos(self):
        monitorSeq = GaudiSequencer('TelaMonitors')
        monitorSeq.IgnoreFilterPassed = True
        monitorSeq.Members += self.getProp('Monitors')
        self.teslaSeq.Members += [ monitorSeq ]
        #
        ApplicationMgr().HistogramPersistency = "ROOT"

        if ( self.getProp("Histogram") != "" ):
            HistogramPersistencySvc().OutputFile = self.getProp("Histogram")

    def _configureTimingAuditor(self):
        """Enable the printing of the algorithm timing table."""
        ApplicationMgr().ExtSvc += ['ToolSvc', 'AuditorSvc']
        ApplicationMgr().AuditAlgorithms = True
        AuditorSvc().Auditors += ['TimingAuditor']

        TimingAuditor().addTool(SequencerTimerTool, name='TIMER')
        TimingAuditor().TIMER.NameSize = 60
        # Disable INFO output from the default timer tool
        SequencerTimerTool().OutputLevel = WARNING

    def _configureTrackTruth(self,assocpp,trackLoc) :
        assoctr = TrackAssociator("TurboAssocTr"+trackLoc.replace("/", ""))
        assoctr.OutputLevel = self.getProp('OutputLevel')
        assoctr.TracksInContainer = trackLoc
        assocpp.TrackLocations += [ trackLoc ]
        # Add it to a sequence
        return assoctr

    def _configureDigitsTruth(self) :
        assocdigits = CaloDigit2MCLinks2Table("TurboDigitAssoc")
        return

    def _configureClustersAndProtosTruth(self,digits,clusters,protos,protoTabLoc):
        retSeq = GaudiSequencer("NeutralTruth")
        clusterTabLoc = self.base + "Relations/CaloClusters"

        assoccluster = CaloClusterMCTruth("TurboClusterAssoc")
        assoccluster.OutputLevel = self.getProp('OutputLevel')
        assoccluster.Input = digits
        assoccluster.Output = clusterTabLoc
        assoccluster.Clusters+=clusters

        assocneutral = NeutralPP2MC("TurboNeutralPP2MC")
        assocneutral.InputData += protos
        assocneutral.OutputLevel = self.getProp('OutputLevel')
        assocneutral.OutputTable = protoTabLoc
        assocneutral.MCCaloTable = clusterTabLoc

        retSeq.Members += [assoccluster,assocneutral]
        return retSeq

    def _unpackMC(self):
        DataOnDemandSvc().NodeMap['/Event/MC']   = 'DataObject'
        DataOnDemandSvc().AlgMap["MC/Particles"] = "UnpackMCParticle"
        DataOnDemandSvc().AlgMap["MC/Vertices"]  = "UnpackMCVertex"


    def _filterMCParticlesSequence(self, relations_locations):
        """Copy signal and associated MC particles to microDST-like locations.

        In the Stripping, the microDST machinery only saves a subset of the
        input MC particles and vertices. This subset contains two possibly
        overlapping contributions:

        1. The MCParticle objects in the signal decay tree; and
        2. The MCParticle objects which can be associated to the reconstructed
        objects selected by at least one stripping line.

        To mimic this behaviour in Tesla, we run almost the same set of
        microDST algorithms, using the relations tables Tesla makes to find the
        MC particles that have been associated to the reconstruction.
        """
        output_prefix = self.base.rstrip('/')
        output_level = self.getProp('OutputLevel')

        # Algorithm to clone the existing relations tables and the MC particles
        # and vertices these tables point to from their original location to
        # the same location prefixed by /Event/Turbo
        copy_pp2mcp = CopyProtoParticle2MCRelations()
        copy_pp2mcp.InputLocations = relations_locations
        copy_pp2mcp.OutputPrefix = output_prefix
        # Don't use the assumed copy of the input ProtoParticle objects, as
        # Tesla doesn't need to copy them (they're already under /Event/Turbo)
        copy_pp2mcp.UseOriginalFrom = True
        copy_pp2mcp.OutputLevel = output_level

        # Algorithm to clone all MC particles and vertices that are associated
        # to the simulated signal process (using LHCb::MCParticle::fromSignal)
        copy_signal_mcp = CopySignalMCParticles()
        copy_signal_mcp.OutputPrefix = output_prefix
        # Don't copy the associated reconstruction, as Tesla already saves it
        copy_signal_mcp.SaveAssociatedRecoInfo = False
        copy_signal_mcp.OutputLevel = output_level

        algorithms = [copy_pp2mcp, copy_signal_mcp]
        seq = GaudiSequencer('TurboMCFiltering', Members=algorithms)

        return seq

    def _configureForOnline(self,stream):
        #
        DecodeRawEvent().DataOnDemand=False
        writer=InputCopyStream( self.writerName+stream )
        self._safeSet(DstConf(), ['SplitRawEventOutput'])

        # we can only do this if we are setting one output writer
        if not self.getProp("Streams"):
            # Use RawEventJuggler to create the Turbo stream raw event format
            tck = "0x409f0045" # DUMMY
            TurboBanksSeq=GaudiSequencer("TurboBanksSeq")
            RawEventJuggler().TCK=tck
            RawEventJuggler().Input="Moore"
            RawEventJuggler().Output=self.getProp("SplitRawEventOutput")
            RawEventJuggler().Sequencer=TurboBanksSeq
            RawEventJuggler().WriterOptItemList=writer
            RawEventJuggler().KillExtraNodes=True
            RawEventJuggler().KillExtraBanks=True
            RawEventJuggler().KillExtraDirectories = True
            if not self.getProp('VetoJuggle'):
                self.teslaSeq.Members += [TurboBanksSeq]

        # we should already have configured the lumiAgls by now
        writer.AcceptAlgs += ["LumiSeq","PhysFilter"]

    def _configureLumi(self):

        # Begin Lumi configuration
        lumiSeq = GaudiSequencer("LumiSeq")
        #
        # Add ODIN decoder to LumiSeq ***
        from DAQSys.Decoders import DecoderDB
        CreateODIN=DecoderDB["createODIN"].setup()
        #********************************
        #
        # Main algorithm config
        lumiCounters = GaudiSequencer("LumiCounters")
        lumiCounters.Members+=[CreateODIN]
        lumiSeq.Members += [ lumiCounters ]

        LumiAlgsConf().LumiSequencer = lumiCounters
        LumiAlgsConf().OutputLevel = self.getProp('OutputLevel')
        LumiAlgsConf().InputType = "MDF"
        #
        # Filter out Lumi only triggers from further processing, but still write to output
        # Trigger masks changed in 2016, see LHCBPS-1486
        physFilterRequireMask = []
        if self.getProp( "DataType" ) in ["2012","2015"]: # 2012 needed for nightlies tests.
            physFilterRequireMask = [ 0x0, 0x4, 0x0 ]
        else:
            physFilterRequireMask = [ 0x0, 0x0, 0x80000000 ]
        physFilter = HltRoutingBitsFilter( "PhysFilter", RequireMask = physFilterRequireMask )

        FSRWriter = RecordStream( "FSROutputStreamDstWriter")
        FSRWriter.OutputLevel = INFO
        self.teslaSeq.Members += [lumiSeq, physFilter]

    def _configureOutput(self):

        TeslaReportAlgoSeq = GaudiSequencer('TeslaReportAlgoSeq')

        lines = self.getProp('TriggerLines')
        turbo_streams = self.getProp('Streams')

        self.teslaSeq.Members += [ TeslaReportAlgoSeq ]

        # Setting up online
        if self.getProp('Mode') is "Online":
            from DAQSys.DecoderClass import decoderToLocation
            from DAQSys.Decoders import DecoderDB
            hlt_locs = [ "Hlt/VertexReports/PV3D", "Hlt2/SelReports","Hlt2/DecReports" ]
            odin=DecoderDB["createODIN"].setup()
            TeslaReportAlgoSeq.Members += [ odin ] + [ decoderToLocation(DecoderDB,loc).setup() for loc in hlt_locs ]

        totalLines=[]
        if turbo_streams:
            lines_left_over = lines[:]
            missing_lines = []
            for stream in turbo_streams:
                for line in turbo_streams[stream]['lines']:
                    totalLines.append(line)
                    try:
                        lines_left_over.remove(line)
                    except ValueError:
                        missing_lines.append(line)
            if self.getProp('ValidateStreams'):
                msg = "Lines missing from streaming:\n  {}\nLines not in turbo but in streams:\n  {}\n".format(
                    '\n  '.join(lines_left_over), '\n  '.join(missing_lines))
                assert lines_left_over==[] and missing_lines==[], msg
                print("Turbo validated")
        else:
            totalLines = lines

        if self.getProp('EnableLineChecker'):
            lineChecker = TeslaLineChecker("TeslaLineChecker")
            lineChecker.RequestedLines = totalLines
            lineChecker.IgnoredLines = self.getProp("IgnoredLines")
            TeslaReportAlgoSeq.Members += [ lineChecker ]
        trig1 = self._configureReportAlg(totalLines)
        TeslaReportAlgoSeq.Members+=[trig1]

        # make use of PrintDuplicates algorithm
        dplic = PrintDuplicates("TurboDuplicates")
        dplic.OutputLevel = self.getProp('OutputLevel')
        for l in totalLines:
            dplic.Inputs+=[self.base + l + "/Particles"]
        if self.getProp('DuplicateCheck'):
            TeslaReportAlgoSeq.Members += [dplic]

        # Make protoparticle -> truth tables *****************************************
        inputType = self.getProp('InputType')
        if (inputType=='XDST') or (inputType=='LDST'):

            # Get Turbo++ locations
            PRtracks=self.getProp("PRtracks")
            PRclusters=self.getProp("PRclusters")
            PRprotos=self.getProp("PRchargedPP")
            PRprotosN=self.getProp("PRneutralPP")
            protoTypes = self.getProp("ProtoTypesPR")

            # CHARGED ################################################################
            ChargedProtoSeq = GaudiSequencer("ChargedTruthSequencer")

            # Standard Turbo locations
            protocont = self.base + "Protos"
            trackcont = self.base + "Tracks"

            assocpp=ChargedPP2MC("TurboProtoAssocPP")
            assocpp.OutputLevel = self.getProp('OutputLevel')
            assocpp.VetoEmpty=True
            # When filtering MC, the relations table cloners will copy the
            # tables to /Event/Turbo for us. Otherwise we create them there
            # directly
            tesROOT = "/Event/"
            if not self.getProp("FilterMC"):
                tesROOT = os.path.join(tesROOT, self.base)
            assocpp.RootInTES=tesROOT
            # Start with an empty input list, so we don't accidentally create
            # relations for non-Turbo/non-PersistReco objects
            assocpp.InputData = []

            # Add Turbo++ track and protoparticle locations
            truthSeqStandard = self._configureTrackTruth(assocpp,trackcont)
            ChargedProtoSeq.Members+=[truthSeqStandard]
            for PRtype in self.getProp("ProtoTypesPR"):
                matchedTracks = [s for s in PRtracks if PRtype in s.lower()]
                matchedProtos = [s for s in PRprotos if PRtype in s.lower()]
                if len(matchedTracks)>1 or len(matchedProtos)>1:
                    raise ValueError("Multiple instances of type "+PRtype+" have been detected")

                trackTruthSeq = self._configureTrackTruth(assocpp,matchedTracks[0])
                print("Adding additional PR protoparticles for truth matching: "+matchedProtos[0])
                assocpp.InputData += [ matchedProtos[0] ]
                ChargedProtoSeq.Members+=[trackTruthSeq]

            # Add container of merged long and downstream PersistReco PPs
            # TODO this should be in the list in the configuration file
            assocpp.InputData += ['Hlt2/Protos/Charged']
            print("Adding additional PR protoparticles for truth matching: "+assocpp.InputData[-1])
            trackTruthSeq = self._configureTrackTruth(assocpp, '/Event/Hlt2/TrackFitted/Charged')
            ChargedProtoSeq.Members += [trackTruthSeq]

            # Add standard Turbo ProtoParticles
            assocpp.InputData += [ protocont ]

            # Add final charged PP2MC algorithm
            ChargedProtoSeq.Members+=[assocpp]

            # NEUTRAL ################################################################
            NeutralProtoSeq = GaudiSequencer("NeutralTruthSequencer")

            ## Add the digits associator
            outputDigiLoc = tesROOT + "Digi2MCP"
            assocdigits = CaloDigit2MCLinks2Table("TurboDigitAssoc")
            assocdigits.OutputLevel = self.getProp('OutputLevel')
            outputiDigiLoc = tesROOT + "Relations/CaloDigits"
            assocdigits.Output = outputDigiLoc

            ## Add the cluster associator
            ## Finally configure the neutral PP2MC associator
            NeutralProtoSeq.Members+=[assocdigits]

            # Gather all protos and clusters
            neutralClustersTot=[]
            neutralProtosTot=[]

            # Add standard Turbo
            neutralClustersTot+=[self.base+"CaloClusters"]
            neutralProtosTot+=[self.base+"Protos"]

            # Add Turbo++
            for clusters in PRclusters:
                neutralClustersTot+=[clusters]
            for protos in PRprotosN:
                neutralProtosTot+=[protos]

            # Configure
            self._configureDigitsTruth()
            protoneutral = self.neutral_pp2mc_loc
            if not self.getProp('FilterMC'):
                protoneutral = os.path.join(tesROOT, protoneutral)
            retSeq = self._configureClustersAndProtosTruth(outputDigiLoc,neutralClustersTot,neutralProtosTot,protoneutral)
            NeutralProtoSeq.Members+=[retSeq]

            # Add standard Turbo locations if not packing
            outputPPLoc = tesROOT+'Relations/Turbo/'
            if not self.getProp('Pack'):
                writer.OptItemList+=[
                        outputPPLoc + 'Protos' + '#99'
                        ]
            if not self.getProp('Pack'):
                writer.OptItemList+=[
                        protoneutral + '#99'
                        ]
            # Add final sequences
            TeslaReportAlgoSeq.Members+=[ChargedProtoSeq,NeutralProtoSeq]

            # Sequence to copy the relations tables, and the subset of MC
            # particles used by those tables, into /Event/Turbo
            if self.getProp("FilterMC"):
                relationsLocations = [os.path.join('Relations', path)
                                      for path in assocpp.InputData]
                relationsLocations.append(protoneutral)
                filterMCSeq = self._filterMCParticlesSequence(relationsLocations)
                TeslaReportAlgoSeq.Members += [filterMCSeq]

        if self.getProp('Pack'):
            packer = PackParticlesAndVertices( name = "TurboPacker",
                    InputStream        = "/Event"+"/"+self.base.rstrip('/'),
                    DeleteInput        = False,
                    EnableCheck        = False,
                    AlwaysCreateOutput = True)
            TeslaReportAlgoSeq.Members +=[packer]

            hypopacker = PackCaloHypos( name = "PackCaloHypos",
                    AlwaysCreateOutput = True,
                    DeleteInput        = False,
                    OutputLevel        = self.getProp('OutputLevel'),
                    InputName          = self.base+"CaloHypos",
                    OutputName         = self.base+"pRec/neutral/Hypos" )
            clusterpacker = PackCaloClusters( name = "PackCaloClusters",
                    AlwaysCreateOutput = True,
                    DeleteInput        = False,
                    OutputLevel        = self.getProp('OutputLevel'),
                    InputName          = self.base+"CaloClusters",
                    OutputName         = self.base+"pRec/neutral/Clusters" )
            TeslaReportAlgoSeq.Members +=[hypopacker,clusterpacker]

            # Pack the filtered MC particles and vertices
            if self.getProp('Simulation') and self.getProp('FilterMC'):
                mcp_packer = PackMCParticle(
                    'TurboPackMCParticle',
                    AlwaysCreateOutput=True,
                    DeleteInput=False,
                    InputName=self.base + 'MC/Particles',
                    OutputName=self.base + 'pSim/MCParticles'
                )
                mcv_packer = PackMCVertex(
                    'TurboPackMCVertex',
                    AlwaysCreateOutput=True,
                    DeleteInput=False,
                    InputName=self.base + 'MC/Vertices',
                    OutputName=self.base + 'pSim/MCVertices'
                )
                TeslaReportAlgoSeq.Members += [mcp_packer, mcv_packer]

            packer.OutputLevel = min(self.getProp('OutputLevel'),self.getProp('PackerOutputLevel'))

        TeslaOutputStreamsSequence = GaudiSequencer('TeslaOutputStreamsSequence')

        TeslaOutputStreamsSequence.ModeOR = True;
        TeslaOutputStreamsSequence.IgnoreFilterPassed = True

        if turbo_streams:
            if self.getProp('Park'):
                streamsOrig = turbo_streams
                for stream in list(streamsOrig.keys()):
                    ps = turbo_streams[stream].get('prescale', None)
                    if ps:
                        newDict={}
                        newDict['lines']=turbo_streams[stream]['lines']
                        pattern = re.compile("prescaled", re.IGNORECASE)
                        if "prescaled" in stream.lower():
                            turbo_streams[pattern.sub("parked", stream)]=newDict
                        else:
                            turbo_streams[stream+"parked"]=newDict
            for stream in turbo_streams:
                if self.getProp('Mode') is "Online":
                    self._configureForOnline(stream)
                TeslaOutputStreamsSequence.Members.append(self._configureOutputStream(stream, turbo_streams[stream]))
        else:
            if self.getProp('Mode') is "Online":
                self._configureForOnline('')
            TeslaOutputStreamsSequence.Members.append(self._configureOutputStream('', {'lines':lines}))

        # Add the output sequence
        TeslaReportAlgoSeq.Members += [TeslaOutputStreamsSequence]

    def _configureTruthMatching(self, name, packing):
        output_level = self.getProp('OutputLevel')
        extra_track_locs = [os.path.join('/Event', self.base, l) for l in _extra_track_locations]
        extra_proto_locs = [os.path.join('/Event', self.base, l) for l in _extra_protoparticle_locations]

        # Locations of the objects we want to truth-match
        track_locs = [packing.outputs['Hlt2LongTracks'],
                      packing.outputs['Hlt2DownstreamTracks']]
        track_locs += extra_track_locs
        proto_locs = [packing.outputs['Hlt2LongProtos'],
                      packing.outputs['Hlt2DownstreamProtos']]
        proto_locs += extra_proto_locs
        cluster_locs = [packing.outputs['Hlt2CaloClusters']]
        neutral_locs = [packing.outputs['Hlt2NeutralProtos']]

        assocpp = ChargedPP2MC("TurboProtoAssocPP")
        # Configure Track -> MCParticle relations table maker
        trackTruthSeq = GaudiSequencer('TurboTrackTruthSequencer')
        trackTruthSeq.Members = [self._configureTrackTruth(assocpp, loc)
                                 for loc in track_locs]

        # Configure ProtoParticle -> MCParticle relations table maker
        assocpp.OutputLevel = output_level
        assocpp.VetoEmpty = True
        assocpp.InputData += _strip_root('/Event', proto_locs)

        assocDigits = CaloDigit2MCLinks2Table("TurboDigitAssoc")
        # The digits relations table maker will try to find the linker table
        # for digits at `Raw/{det}/Digits` at `Link/Raw/{det}/Digits`. Our
        # digits are at `Turbo/Raw/{det}/Digits`, but we can't remake a linker
        # table at `Link/Turbo/Raw/{det}/Digits because the information needed
        # to make one is lost after Boole.
        # Instead, exploit the fact that the digits under `Turbo/` are a subset
        # of the originals, and just symlink the original linker table to the
        # location expected for the Turbo digits
        assocDigits.RespectInputDigitLocation = False
        digit_locs = _strip_root('/Event', assocDigits.getDefaultProperty('Inputs'))
        turbo_digit_locs = [os.path.join(self.base, l) for l in digit_locs]
        linkingSeq = GaudiSequencer('TurboLinkTableLinkers')
        for digit_loc in digit_locs:
            link_loc = os.path.join('Link', digit_loc)
            turbo_link_loc = os.path.join('Link', self.base, digit_loc)
            name = '{0}Linker'.format(link_loc.replace('/', ''))
            dl = DataLink(name, What=link_loc, Target=turbo_link_loc)
            linkingSeq.Members.append(dl)
            # DataLinks don't create the ancestor TES structure, so do it
            # manually
            path = ''
            for node in turbo_link_loc.split(os.path.sep)[:-1]:
                path = os.path.join(path, node)
                DataOnDemandSvc().NodeMap[path] = 'DataObject'

        # Configure CaloDigit -> MCParticle relations table maker
        assocDigits.OutputLevel = output_level
        # Can use a magic string here as this table is only temporary
        assocDigits.Output = "Relations/CaloDigits"
        assocDigits.Inputs = turbo_digit_locs

        # Configure CaloCluster -> MCParticle and neutral ProtoParticle ->
        # MCParticle relations table makers
        neutral_rels_loc = self.neutral_pp2mc_loc
        assocClustersNeutrals = self._configureClustersAndProtosTruth(
            assocDigits.Output,
            cluster_locs,
            neutral_locs,
            neutral_rels_loc
        )

        chargedProtoSeq = GaudiSequencer("ChargedTruthSequencer")
        chargedProtoSeq.Members = [trackTruthSeq, assocpp]
        neutralProtoSeq = GaudiSequencer("NeutralTruthSequencer")
        neutralProtoSeq.Members = [linkingSeq, assocDigits, assocClustersNeutrals]
        truthSeq = GaudiSequencer("TurboTruthSequencer")
        truthSeq.Members = [chargedProtoSeq, neutralProtoSeq]

        # We have now created the relations tables under /Event/Relations,
        # and want them under /Event/Turbo/Relations. We have two strategies:
        #   1. Use the CopyProtoParticle2MCRelations algorithm, which will copy
        #   the tables *and* the MC particles they reference to under
        #   /Event/Turbo, updating the copied tables to point to the copied MC
        #   particles; or
        #   2. Use Gaudi::DataCopy to copy the tables verbatim, so that they
        #   still refer to MC objects in /Event/MC.
        # The first case we call 'filtering', as in principle the original MC
        # objects can now be deleted because we then only depend on those in
        # /Event/Turbo/MC. The second case we choose to do when not filtering
        # so that the paths to the final relations tables are the same as those
        # in the first case.
        relationsLocations = [os.path.join('Relations', path)
                              for path in assocpp.InputData]
        relationsLocations.append(neutral_rels_loc)
        if self.getProp('FilterMC'):
            filterMCSeq = self._filterMCParticlesSequence(relationsLocations)
            truthSeq.Members += [filterMCSeq]
        else:
            base = self.base.rstrip('/')
            truthSeq.Members.append(
                GaudiSequencer('CopyRelationsTables', Members=[
                    DataCopy('Copy{0}'.format(loc.replace('/', '')),
                             What=loc,
                             Target=os.path.join(base, loc),
                             # It's very possible that the extra locations
                             # might not exist, so no relations table will be
                             # created; we don't mind the copying failing in
                             # those cases
                             NeverFail=True if loc.replace('Relations/', '/Event/') in (extra_track_locs + extra_proto_locs) else False,
                             OutputLevel=output_level)
                    for loc in relationsLocations
                    if self.base in loc
                ], IgnoreFilterPassed=True)
            )

        return truthSeq


    def _configureOutputStream(self, stream, lines_dict):
        '''
        The lines dictionary should have the following form:

        lines: The lines to include in the stream
        prescale: The amount to prescale, as a dictionary of lines. Uses the Prescale property if not present or None
        prescaleVersion: A version number
        park: The amount to park. A second line will be created, with the given amound and the label 'Park' appended (not implemented)

        Only the first key is required. The properties will be taken from self otherwise.

        Returns: a stream sequence
        '''


        # Building output name
        fname =""
        if not stream:
            fname = self.getProp("outputFile")
        else:
            fname = self.getProp("outputPrefix") + stream + self.getProp("outputSuffix")

        # Building sequencer with stream
        seq = GaudiSequencer('TeslaStreamSequence' + stream)

        # Required structure
        lines = lines_dict['lines']

        # Optional prescaling
        prescale = lines_dict.get('prescale', None)
        turboRepLoc="Turbo/DecReports"+stream
        if prescale:
            print "Requested to prescale stream: "+stream
            # if line not in dictionary, give it a prescale of 1
            for line in lines:
                if line not in prescale:
                    prescale[line]=1.0
            print prescale
            prescaleVersion = lines_dict.get('prescaleVersion', None)
            pscaler = TurboPrescaler("TurboPrescaler"+stream)
            pscaler.FilterOutput=True
            pscaler.ChosenOutputPrescales=prescale
            pscaler.PrescaleVersion=prescaleVersion
            pscaler.HltDecReportsLocation="Hlt2/DecReports"
            pscaler.ReportsOutputLoc=turboRepLoc
            seq.Members+=[pscaler]

            # Write new reports to the raw event
            #from Configurables import HltDecReportsWriter
            #decWriter = HltDecReportsWriter("Hlt2DecReportsWriter")
            #decWriter.SourceID=3
            #decWriter.InputHltDecReportsLocation=turboRepLoc
            #seq.Members+=[decWriter]

        #retrieve the persistency
        persistency=None
        if hasattr(self, "Persistency"):
            if self.getProp("Persistency") is not None:
                persistency=self.getProp("Persistency")

        iox=IOExtension(persistency)

        writer = InputCopyStream(self.writerName + stream)
        writer.OutputLevel = self.getProp('OutputLevel')

        writer.RequireAlgs += [seq]

        # If HDR requested, then filter on the output (required if streaming)
        if self.getProp("HDRFilter") or self.getProp("Streams"):
            filtCode='|'.join(["HLT_TURBOPASS_RE(\'"+l+"Decision\')" for l in lines])
            print("Filter code:")
            print(filtCode)
            HLTFilter = HltDecReportsFilter ('Hlt2_HLTFilter' + stream
                    , Code  = filtCode
                    , Location = "Hlt2/DecReports"
                    )
            writer.RequireAlgs+=[HLTFilter]
            seq.Members+=[HLTFilter]

        # Add our prescaled reports if present
        if prescale:
            writer.OptItemList+=[ turboRepLoc + "#99"]

        for l in lines:
            # IF NOT PACKING NEED TO SET THE LOCATIONS IN THE REPORT ALGORITHM
            if not self.getProp('Pack'):
                writer.OptItemList+=[ self.base + l + "/Particles#99"
                        , self.base + l + "/Vertices#99"
                        , self.base + l + "/_ReFitPVs#99"
                        , self.base + l + "/Particle2VertexRelations#99"
                        ]

        if not self.getProp('Pack'):
            writer.OptItemList+=[
                self.base + "Protos#99"
                ,self.base + "Tracks#99"
                ,self.base + "RichPIDs#99"
                ,self.base + "MuonPIDs#99"
                ,self.base + "CaloHypos#99"
                ,self.base + "CaloClusters#99"
                ]


        # Does this need to be in streaming? HENRYIII
        # Add shared places to writer
        # SB: Might as well support non-packing case.
        if not self.getProp('Pack'):
            writer.OptItemList+=[
                    self.base + "Primary#99"
                    , self.base + "Rec/Summary#99"
                    ]

        if self.getProp('Pack'):
            writer.OptItemList+=[
                    self.base+"pPhys/Particles#99"
                    ,self.base+"pPhys/Vertices#99"
                    ,self.base+"pPhys/RecVertices#99"
                    ,self.base+"pPhys/Relations#99"
                    ,self.base+"pPhys/PP2MCPRelations#99"
                    ,self.base+"pPhys/PartToRelatedInfoRelations#99"
                    ,self.base+"pRec/Track/Custom#99"
                    ,self.base+"pRec/Muon/CustomPIDs#99"
                    ,self.base+"pRec/Rich/CustomPIDs#99"
                    ,self.base+"pRec/ProtoP/Custom#99"
                    ,self.base+"pRec/neutral/Hypos#99"
                    ,self.base+"pRec/neutral/Clusters#99"
                    ,self.base+"Rec/Summary#99"
                    ,self.base+"pSim/MCParticles#99"
                    ,self.base+"pSim/MCVertices#99"
                    ]

        IOHelper(persistency,persistency).outStream(fname,writer,writeFSR=self.getProp('WriteFSR'))
        return seq

    def _configureReportAlg(self, lines):
        trig1 = TeslaReportAlgo("TeslaReportAlgo")
        trig1.OutputPrefix=self.base
        trig1.PV=self.getProp('PV')
        if self.getProp('VertRepLoc')=="Hlt1":
            trig1.VertRepLoc="Hlt1/VertexReports"
        else:
            trig1.VertRepLoc="Hlt2/VertexReports"
        trig1.PVLoc=self.base+"Primary"
        trig1.TriggerLines=lines
        trig1.OutputLevel=self.getProp('OutputLevel')
        return trig1

    def _selReportsCheck(self, loc='Hlt2/SelReports'):
        name = '{0}Check'.format(loc.replace('/', ''))
        return TESCheck(name, Inputs=[loc], Stop=False)

    def _configureOutputTurboSP(self):
        trigger_lines = self.getProp('TriggerLines')
        streams = self.getProp('Streams')
        simulation = self.getProp('Simulation')
        datatype = self.getProp('DataType')
        mode = self.getProp('Mode')
        online = mode == 'Online'
        vertex_report_location = self.getProp('VertRepLoc')
        enable_line_checker = self.getProp('EnableLineChecker')
        raw_format_input = self.getProp('SplitRawEventInput')
        raw_format_output = self.getProp('SplitRawEventOutput')

        ok = (trigger_lines and not streams) or (streams and not trigger_lines)
        assert ok, ('Must define at least one of Tesla().Streams and '
                    'Tesla().TriggerLines, but not both')

        # Treat the no-streaming case as a single anonymous stream
        if not streams:
            streams = {'': {'lines': trigger_lines}}

        decoders_seq = GaudiSequencer('TeslaDecoders')
        if online:
            from DAQSys.DecoderClass import decoderToLocation
            from DAQSys.Decoders import DecoderDB
            hlt_locs = [ "Hlt/VertexReports/PV3D", "Hlt2/SelReports","Hlt2/DecReports" ]
            odin=DecoderDB["createODIN"].setup()
            decoders_seq.Members += [ odin ] + [ decoderToLocation(DecoderDB,loc).setup() for loc in hlt_locs ]            
            DecodeRawEvent().DataOnDemand = False
            self._safeSet(DstConf(), ['SplitRawEventOutput'])

        # This decoder is setup by TurboConf
        packed_data_decoder = HltPackedDataDecoder('Hlt2PackedDataDecoder')
        decoders_seq.Members.append(packed_data_decoder)

        prpacking = PersistRecoConf.PersistRecoPacking(datatype)
        prunpackers = prpacking.unpackers()

        unpackers_seq = GaudiSequencer('TeslaUnpackers')
        # Need to run the packers in reverse order, due to object dependencies
        unpackers_seq.Members += prunpackers[::-1]
        unpack_psandvs = UnpackParticlesAndVertices(
            InputStream='/Event/Turbo'
        )
        unpackers_seq.Members.append(unpack_psandvs)

        stream_sequences = []
        for stream_name in streams:
            stream_seq = self._configureOutputTurboSPStream(
                stream_name,
                streams[stream_name]['lines'],
                prpacking,
                online,
                raw_format_output
            )
            stream_sequences.append(stream_seq)

        if enable_line_checker:
            all_lines = sum([d['lines'] for d in streams.values()], [])
            ignored_lines = self.getProp('IgnoredLines')
            line_checker = [TeslaLineChecker(
                RequestedLines=all_lines,
                IgnoredLines=ignored_lines
            )]
        else:
            line_checker = []

        # Kill the input DstData bank, which is in DAQ/RawEvent online, but in
        # PersistReco/RawEvent offline (e.g. after Brunel)
        if online:
            dstdata_killer = [bankKiller(
                'Hlt2DstDataKiller',
                BankTypes=['DstData']
            )]
        else:
            dstdata_killer = [EventNodeKiller(
                'Hlt2DstDataKiller',
                Nodes=['PersistReco']
            )]

        # Kill any links the output file might have had to the input file
        if online and not simulation:
            address_killer = [AddressKillerAlg()]
        else:
            address_killer = []

        streaming_seq = GaudiSequencer(
            'TeslaStreamsSequence',
            Members=(
                [decoders_seq, unpackers_seq] +
                line_checker +
                stream_sequences +
                dstdata_killer +
                address_killer
            ),
            IgnoreFilterPassed=True
        )

        return streaming_seq

    def _configureOutputTurboSPStream(self, name, lines, packing, online,
                                      raw_format_output):
        """Return a sequence for streaming the lines into the named location.

        Copy line outputs for this stream to a stream-specific location.

        Keyword arguments:
        name -- Name of the stream
        lines -- List of HLT2 line names that belong in the stream
        packing -- Instance of PersistRecoConf.PersistRecoPacking
        online -- Configure in online mode (if True) or offline
        raw_format_output -- Output format of the raw event
        """
        output_prefix = name.title()
        decisions = [l + 'Decision' for l in lines]
        # Naming convention for stream-specific algorithms
        namer = lambda x: '{0}ForStream{1}'.format(x, output_prefix)
        pack = self.getProp('Pack')
        write_fsr = self.getProp('WriteFSR')
        simulation = self.getProp('Simulation')
        filter_mc = self.getProp('FilterMC')
        if filter_mc:
            assert simulation, 'Expected Simulation = True as FilterMC = True'

        stream_seq = GaudiSequencer(namer('TeslaStreamSequence'))

        # Run the stream sequence iff at least one line in the stream fired
        filter_code = '|'.join([
            "HLT_TURBOPASS_RE('{0}')".format(l) for l in decisions
        ])
        filter = HltDecReportsFilter(
            namer('Hlt2_HLTFilter'),
            Code=filter_code
        )
        # Don't filter an anonymous stream by default
        if output_prefix or self.getProp('HDRFilter'):
            stream_seq.Members.append(filter)

        tes_root = '/Event'
        # /Event/<stream name>
        stream_base = os.path.join(tes_root, output_prefix).rstrip('/')
        # /Event/<stream name>/Turbo
        turbo_base = os.path.join(stream_base, 'Turbo')

        # Decode the RecSummary from the SelReports
        recsummary_decoder = RecSummaryFromSelReports(namer('RecSummaryFromSelReports'))
        recsummary_decoder.InputHltSelReportsLocation = 'Hlt2/SelReports'
        recsummary_decoder.OutputRecSummaryLocation = os.path.join(turbo_base, 'Rec/Summary')
        stream_seq.Members.append(GaudiSequencer(
            namer('RecSummarySeq'), Members=[
                self._selReportsCheck(),
                recsummary_decoder
            ]
        ))

        if simulation:
            self._unpackMC()
            stream_seq.Members.append(self._configureTruthMatching(name, packing))

        # No need to clone if the output prefix is empty (everything stays
        # under /Event/Turbo)
        if output_prefix:
            persistence_svc = self.getProp('ILinePersistenceSvc')
            ApplicationMgr().ExtSvc.append(persistence_svc)
            # Tell the persistence service to map any packed locations it knows
            # about to unpacked locations, as it is the unpacked locations we
            # want to copy to stream-specific locations
            TCKLinePersistenceSvc().ContainerMap = packing.packedToOutputLocationMap()

            container_cloner = CopyLinePersistenceLocations(
                namer('CopyLinePersistenceLocations'),
                OutputPrefix=output_prefix,
                LinesToCopy=decisions,
                ILinePersistenceSvc=persistence_svc
            )
            ppc = container_cloner.addTool(ProtoParticleCloner)
            tcwc = container_cloner.addTool(TrackClonerWithClusters)
            cdc = container_cloner.addTool(CaloDigitCloner)

            # Use the Track cloner that copies associated track clusters
            ppc.ICloneTrack = "TrackClonerWithClusters"
            tcwc.CloneAncestors = False

            # We do two things here:
            #   1. Configure the CaloCluster and CaloHypo cloners to have
            #   consistent behaviour;
            #   2. Force digits to always be cloned when running over simulated
            #   data.
            ccc = container_cloner.addTool(CaloClusterCloner, name='CaloClusterCloner')
            chc = container_cloner.addTool(CaloHypoCloner, name='CaloHypoCloner')
            ccc.CloneEntriesNeuP = True
            ccc.CloneEntriesChP = False
            ccc.CloneEntriesAlways = simulation
            chc.CloneClustersNeuP = True
            chc.CloneClustersChP = False
            chc.CloneDigitsNeuP = True
            chc.CloneDigitsChP = False
            chc.CloneClustersAlways = simulation

            # We *always* want associated digits to be cloned in PersistReco
            ccc_pp = ccc.clone("CaloClusterClonerForTurboPP")
            ccc_pp.CloneEntriesAlways = True
            container_cloner.TurboPPICloneCaloCluster = container_cloner.addTool(ccc_pp).getFullName()
            chc_pp = chc.clone("CaloHypoClonerForTurboPP")
            chc_pp.CloneClustersAlways = True
            container_cloner.TurboPPICloneCaloHypo = container_cloner.addTool(chc_pp).getFullName()

            # Look for input objects under /Event/Turbo
            def prepend_base(configurable, property):
                """Prepend Tesla.base to the configurable's property value."""
                default = configurable.getDefaultProperty(property)
                value = os.path.join(self.base, default)
                configurable.setProp(property, value)
            prepend_base(tcwc, 'VeloClusters')
            prepend_base(tcwc, 'TTClusters')
            prepend_base(tcwc, 'ITClusters')
            prepend_base(cdc, 'EcalADCLocation')
            prepend_base(cdc, 'PrsADCLocation')

            p2pv_cloner = CopyParticle2PVRelationsFromLinePersistenceLocations(
                namer('CopyP2PVRelationsFromLinePersistenceLocations'),
                OutputPrefix=output_prefix,
                LinesToCopy=decisions,
                ILinePersistenceSvc=persistence_svc
            )

            p2ri_cloner = CopyParticle2RelatedInfoFromLinePersistenceLocations(
                namer('CopyP2RelatedInfoFromLinePersistenceLocations'),
                # Search for all LHCb::RelatedInfoMap objects at the same TES
                # level as the container of LHCb::Particle objects using the
                # CLID, rather than a name
                RelationsBaseName="",
                UseRelationsCLID=True,
                OutputPrefix=output_prefix,
                LinesToCopy=decisions,
                ILinePersistenceSvc=persistence_svc
            )

            copy_line_outputs_seq = GaudiSequencer(
                namer('TeslaCopyLineOutputsSequence'),
                Members=[
                    container_cloner,
                    p2pv_cloner,
                    p2ri_cloner
                ],
                IgnoreFilterPassed=True
            )
            stream_seq.Members.append(copy_line_outputs_seq)

        required_output_locations = [
            str(recsummary_decoder.OutputRecSummaryLocation) + '#1'
        ]
        optional_output_locations = []
        if pack:
            datatype = self.getProp('DataType')
            packers = []

            # Pack the PersistReco locations for this stream
            prpacking_inputs = {
                k: v.replace(tes_root, stream_base)
                for k, v in packing.outputs.items()
            }
            prpacking_descriptors = {
                datatype: {
                    k: d.copy(location=d.location.replace(tes_root, turbo_base))
                    for k, d in PersistRecoConf.standardDescriptors[datatype].items()
                }
            }
            prpacking_stream = PersistRecoConf.PersistRecoPacking(
                datatype,
                inputs=prpacking_inputs,
                descriptors=prpacking_descriptors
            )
            packers += prpacking_stream.packers(identifier=namer(''))

            # Pack everything else that we've cloned
            psandvs_packer = PackParticlesAndVertices(
                namer('TurboPacker'),
                InputStream=turbo_base,
                VetoedContainers=prpacking_stream.inputs.values()
            )
            packers.append(psandvs_packer)

            # Pack the filtered MC particles and vertices
            if filter_mc:
                mcp_packer = PackMCParticle(
                    'TurboPackMCParticle',
                    AlwaysCreateOutput=True,
                    DeleteInput=False,
                    RootInTES=turbo_base
                )
                mcv_packer = PackMCVertex(
                    'TurboPackMCVertex',
                    AlwaysCreateOutput=True,
                    DeleteInput=False,
                    RootInTES=turbo_base
                )

                packers.append(mcp_packer)
                packers.append(mcv_packer)

                optional_output_locations += [os.path.join(turbo_base, 'pSim#*')]

            for packer in packers:
                packer.OutputLevel = min(self.getProp('OutputLevel'),
                                         self.getProp('PackerOutputLevel'))

            # If the stream is anonymous, we must first kill the output of
            # PackParticlesAndVertices from HLT2 (which lives in the same
            # place) so that we can re-run the algorithm here
            # This is done primarily for packing relations tables, which are
            # packed by PackParticlesAndVertices but created only in Tesla
            node_killers = []
            if not output_prefix:
                enk = EventNodeKiller(namer('KillHLT2PackPsAndVsOutput'))
                # List of locations produced by PackParticlesAndVertices
                enk.Nodes = [os.path.join(turbo_base, p) for p in [
                    'pPhys/Particles',
                    'pPhys/Vertices',
                    'pPhys/FlavourTags',
                    'pPhys/RecVertices',
                    'pPhys/Relations',
                    'pPhys/P2MCPRelations',
                    'pPhys/P2IntRelations',
                    'pPhys/PartToRelatedInfoRelations',
                    'pRec/ProtoP/Custom',
                    'pRec/Muon/CustomPIDs',
                    'pRec/Rich/CustomPIDs',
                    'pRec/Track/Custom',
                    'pPhys/PP2MCPRelations'
                ]]
                node_killers.append(enk)

            packing_seq = GaudiSequencer(
                namer('TeslaPackingSequence'),
                Members=node_killers + packers,
                IgnoreFilterPassed=True
            )
            stream_seq.Members.append(packing_seq)

            optional_output_locations += [
                os.path.join(turbo_base, 'pPhys#*'),
                os.path.join(turbo_base, 'pRec#*'),
                os.path.join(turbo_base, 'Hlt2/pRec#*')
            ]
        else:
            # Save everything under the stream prefix
            optional_output_locations += [
                os.path.join(turbo_base, '#*')
            ]

        if output_prefix:
            fname = '{0}{1}{2}'.format(
                self.getProp('outputPrefix'),
                output_prefix,
                self.getProp('outputSuffix')
            )
        else:
            fname = self.getProp('outputFile')

        if online and not simulation:
            writer = OutputStream(namer(self.writerName))
            writer.AcceptAlgs += ['LumiSeq', 'PhysFilter']
            # In online mode we perform the raw event juggling (otherwise
            # Brunel does it for us), so must save the locations after juggling
            optional_output_locations += self._output_raw_event_locations(raw_format_output)
        else:
            # In offline mode, e.g. after Brunel, propagate everything from the
            # input file to the output
            writer = InputCopyStream(namer(self.writerName))

        writer.ItemList = required_output_locations
        writer.OptItemList = optional_output_locations

        # Keep the 2016 behaviour by not writing out anything if the
        # sel reports are missing
        writer.RequireAlgs = [self._selReportsCheck(), stream_seq]
        iohelper = IOHelper()
        iohelper.outStream(fname, writer, writeFSR=write_fsr)

        return stream_seq

    def _configureHlt2SelReportsKill(self):
        from Configurables import bankKiller, HltSelReportsStripper

        inputHlt2RepsToKeep = self.getProp('InputHlt2RepsToKeep')

        kill_seq = GaudiSequencer('Hlt2SelReportsKillerSeq')

        killer = bankKiller("Hlt2SelReportsBankKiller")
        killer.BankTypes = ["HltSelReports"]
        killer.DefaultIsKill = False
        # HltSelReportsWriter::kSourceID_Hlt2 << HltSelReportsWriter::kSourceID_BitShift
        killer.KillSourceID = 2 << 13
        # HltSelReportsWriter::kSourceID_MajorMask
        killer.KillSourceIDMask = 0xE000
        kill_seq.Members.append(killer)

        # if the list of reports to keep is empty, do nothing more
        if inputHlt2RepsToKeep:
            assert all(name.endswith('Decision') for name in inputHlt2RepsToKeep)

            from DAQSys.Decoders import DecoderDB
            selreports_decoder = DecoderDB['HltSelReportsDecoder/Hlt2SelReportsDecoder']
            selreports_loc = selreports_decoder.Outputs['OutputHltSelReportsLocation']
            decreports_decoder = DecoderDB['HltDecReportsDecoder/Hlt2DecReportsDecoder']
            decreports_loc = decreports_decoder.Outputs['OutputHltDecReportsLocation']

            # Stop the sequence if Hlt2/SelReports does not exist
            kill_seq.Members.append(self._selReportsCheck())

            stripper = HltSelReportsStripper('Hlt2SelReportsStripper')
            stripper.InputHltSelReportsLocation = selreports_loc
            stripper.OutputHltSelReportsLocation = 'Hlt2/SelReportsFiltered'
            stripper.OutputHltObjectSummariesLocation = (
                str(stripper.OutputHltSelReportsLocation) + '/Candidates')
            stripper.SelectionNames = inputHlt2RepsToKeep
            stripper.OutputLevel = self.getProp('OutputLevel')
            kill_seq.Members.append(stripper)

            writer = HltSelReportsWriter('Hlt2SelReportsFilteredWriter')
            writer.SourceID = 2
            writer.InputHltSelReportsLocation = str(stripper.OutputHltSelReportsLocation)
            writer.OutputLevel = self.getProp('OutputLevel')
            # By the time the stripper runs Hlt2/DecReports should exist
            writer.UseTCK = True  # use the TCKANNSvc for converting names to numbers
            writer.InputHltDecReportsLocation = decreports_loc
            kill_seq.Members.append(writer)

        return kill_seq

    def _raw_event_juggler(self, input_format, output_format):
        # Load the raw event format dictionaries
        RawEventFormatConf().loadIfRequired()

        j = RawEventJuggler()
        j.Input = input_format
        j.Output = output_format
        j.KillExtraNodes = True
        j.KillExtraBanks = True
        j.KillExtraDirectories = True
        j.Sequencer = GaudiSequencer('TeslaRawEventJugglerSequence')

        return j.Sequencer

    def _output_raw_event_locations(self, output_format):
        """Return the list of raw event locations for the output format."""
        # Load the raw event format dictionaries
        RawEventFormatConf().loadIfRequired()

        locs = RawEventLocationsToBanks(output_format).keys()
        return [os.path.join('/Event', l) + '#1' for l in locs]

    def __apply_configuration__(self):
        ############## Set other properties ###########
        self._safeSet( LHCbApp(), ['EvtMax','SkipEvents','Simulation', 'DataType' , 'CondDBtag','DDDBtag'] )
        self._safeSet( TurboConf(), ['DataType'] )

        # Make sure DQFlags are ignored for Tesla productions
        if self.getProp('IgnoreDQFlags'):
            if not LHCbApp().isPropertySet( "IgnoreDQFlags" ) :
                LHCbApp().setProp( "IgnoreDQFlags", True )

        # Silence, please!
        LoKiSvc().Welcome = False

        ApplicationMgr().AppName="Tesla, utilising DaVinci"
        #
        if self.getProp('Mode') == "Online":
            self.setProp('WriteFSR',True)
            if not self.getProp('Simulation'):
                self._configureLumi()
        else:
            DecodeRawEvent().DataOnDemand=True
            RecombineRawEvent(Version=self.getProp('SplitRawEventInput'))
            if self.getProp('DataType') in ['2015', '2016'] and self.getProp('Simulation')==True:
                self._unpackMC()
                TurboConf().setProp("PersistReco",True)
                # We don't want the PersistReco reconstruction under
                # /Event/Turbo, else it will be packed
                TurboConf().setProp("RootInTES", "/Event")

        # The MC logic requires the DataOnDemandSvc to be enabled
        if self.getProp('Simulation'):
            self._configureDataOnDemand()

        if self.getProp('DataType') in ['2017', '2018'] and not self.getProp('CandidatesFromSelReports'):
            # Unpack the DstData raw bank
            TurboConf().setProp("RunPackedDataDecoder", True)
            streaming_seq = self._configureOutputTurboSP()
            self.teslaSeq.Members.append(streaming_seq)
        else:
            self._configureOutput()
        #
        EventSelector().PrintFreq = 1000

        # Add monitors if they are there
        if len(self.getProp('Monitors'))>0:
            self._configureHistos()

        # Enable the timing table
        self._configureTimingAuditor()

        if self.getProp('KillInputTurbo'):
            enk = EventNodeKiller('KillTurbo')
            enk.Nodes = [ "Turbo" ]
            ApplicationMgr().TopAlg.insert( 0,  enk.getFullName() )

        if self.getProp('KillInputHlt2Reps'):
            kill_selreports = self._configureHlt2SelReportsKill()
            self.teslaSeq.Members += [kill_selreports]

        if self.getProp('SplitRawEventInput') != self.getProp('SplitRawEventOutput'):
            raw_event_juggler = self._raw_event_juggler(
                self.getProp('SplitRawEventInput'),
                self.getProp('SplitRawEventOutput')
            )
            self.teslaSeq.Members += [raw_event_juggler]

        ApplicationMgr().TopAlg+=[self.teslaSeq]
