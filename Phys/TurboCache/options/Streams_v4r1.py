###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from TurboStreamProd.streams import turbo_streams
from Tesla.Configuration import Tesla
Tesla().Streams = turbo_streams["2016"]

#Tesla().ValidateStreams = True
# Turn on Sascha's algorithm ignoring TurboCalib
Tesla().EnableLineChecker = True
Tesla().IgnoredLines = [".*TurboCalib"]
