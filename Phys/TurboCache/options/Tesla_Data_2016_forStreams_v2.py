###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Tesla.Configuration import Tesla
Tesla().Pack = True
Tesla().InputType = "RAW"
Tesla().DataType = '2016'
Tesla().Simulation = False
Tesla().Mode = 'Online'
Tesla().RawFormatVersion = 0.2
Tesla().VertRepLoc = 'Hlt2'
Tesla().Park=True
Tesla().KillInputHlt2Reps=True

Tesla().outputSuffix = ".mdst"
#Tesla().outputPrefix = "TURBO_"

### Already juggled online
Tesla().VetoJuggle = True

Tesla().HDRFilter=True
