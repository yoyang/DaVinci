2019-08-01 DaVinci v50r5
========================

Development release prepared on the master branch.

It is based on Gaudi v32r1, LHCb v50r5, Lbcom v30r5, Rec v30r5, Phys v30r5, Analysis v30r5 and Stripping v15r0.
It uses LCG_96 with ROOT 6.18.00.

- Update of DaVinciTests' test_standardintermediate_reco14_run.qmt
  - See merge request lhcb/DaVinci!332
- Remove TrackSys from DaVinci and Tesla configurables
  - See merge request lhcb/DaVinci!331
