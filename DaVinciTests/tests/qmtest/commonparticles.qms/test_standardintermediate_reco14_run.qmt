<?xml version="1.0" ?>
<!--
    (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="program"><text>gaudirun.py</text></argument>
<argument name="timeout"><integer>6400</integer></argument>
<argument name="args"><set>
  <text>../options/CommonParticles/DVTestStandardIntermediate.py</text>
  <text>$PRCONFIGOPTS/DaVinci/Stripping/Collision12-4TeV-Reco14-FULLDST.py</text>
</set></argument>
<argument name="prerequisites"><set>
  <tuple><text>commonparticles.test_standardbasic_reco14_init</text><enumeral>PASS</enumeral></tuple>
  <tuple><text>commonparticles.test_standardintermediate_reco14_init</text><enumeral>PASS</enumeral></tuple>
</set></argument>
<argument name="validator"><text>
findReferenceBlock("""
DaVinciInitAlg                                              SUCCESS 500 events processed
DaVinciInitAlg                                              SUCCESS ==================================================================
CountParticles                                              SUCCESS Number of counters : 97
 |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |
 | "# Phys/StdAllLooseMuPion"                      |       500 |      11668 |     23.336 |     51.196 |      0.0000 |      541.00 |
 | "# Phys/StdDiElectronFromTracks"                |       500 |      11976 |     23.952 |     41.569 |      0.0000 |      378.00 |
 | "# Phys/StdDiElectronGamma"                     |       500 |      11988 |     23.976 |     41.603 |      0.0000 |      378.00 |
 | "# Phys/StdKs2PiPiDD"                           |       500 |        177 |    0.35400 |    0.66984 |      0.0000 |      4.0000 |
 | "# Phys/StdKs2PiPiLL"                           |       500 |        319 |    0.63800 |    0.99144 |      0.0000 |      8.0000 |
 | "# Phys/StdLTUnbiasedJpsi2MuMu"                 |       500 |         13 |   0.026000 |    0.15914 |      0.0000 |      1.0000 |
 | "# Phys/StdLambda2PPiDD"                        |       500 |         58 |    0.11600 |    0.35573 |      0.0000 |      3.0000 |
 | "# Phys/StdLambda2PPiLL"                        |       500 |        139 |    0.27800 |    0.61377 |      0.0000 |      4.0000 |
 | "# Phys/StdLooseD02KK"                          |       500 |        952 |     1.9040 |     6.1272 |      0.0000 |      64.000 |
 | "# Phys/StdLooseD02KPi"                         |       500 |       1143 |     2.2860 |     6.5946 |      0.0000 |      74.000 |
 | "# Phys/StdLooseD02KPiDCS"                      |       500 |       1143 |     2.2860 |     6.5946 |      0.0000 |      74.000 |
 | "# Phys/StdLooseD02KsKK"                        |       500 |        100 |    0.20000 |     2.0337 |      0.0000 |      42.000 |
 | "# Phys/StdLooseD02KsPiPi"                      |       500 |        108 |    0.21600 |     1.0589 |      0.0000 |      14.000 |
 | "# Phys/StdLooseD02PiPi"                        |       500 |       1134 |     2.2680 |     6.0147 |      0.0000 |      78.000 |
 | "# Phys/StdLooseDalitzPi0"                      |       500 |        117 |    0.23400 |    0.88501 |      0.0000 |      10.000 |
 | "# Phys/StdLooseDetachedDiElectron"             |       500 |        593 |     1.1860 |     2.3982 |      0.0000 |      19.000 |
 | "# Phys/StdLooseDetachedDiElectronLU"           |       500 |        810 |     1.6200 |     8.5800 |      0.0000 |      186.00 |
 | "# Phys/StdLooseDetachedDipion"                 |       500 |       3193 |     6.3860 |     7.4614 |      0.0000 |      59.000 |
 | "# Phys/StdLooseDetachedKpi"                    |       500 |      27494 |     54.988 |     116.83 |      0.0000 |      1231.0 |
 | "# Phys/StdLooseDetachedKst2Kpi"                |       500 |      10255 |     20.510 |     41.454 |      0.0000 |      437.00 |
 | "# Phys/StdLooseDetachedPhi2KK"                 |       500 |        914 |     1.8280 |     4.7932 |      0.0000 |      48.000 |
 | "# Phys/StdLooseDetachedTau3pi"                 |       500 |       1337 |     2.6740 |     7.3506 |      0.0000 |      108.00 |
 | "# Phys/StdLooseDetachedTau3piNonPhys"          |       500 |        343 |    0.68600 |     1.9109 |      0.0000 |      21.000 |
 | "# Phys/StdLooseDiElectron"                     |       500 |        322 |    0.64400 |     1.7903 |      0.0000 |      14.000 |
 | "# Phys/StdLooseDiMuon"                         |       500 |       1603 |     3.2060 |     10.551 |      0.0000 |      134.00 |
 | "# Phys/StdLooseDiMuonSameSign"                 |       500 |       1944 |     3.8880 |     10.777 |      0.0000 |      114.00 |
 | "# Phys/StdLooseDplus2KKK"                      |       500 |          4 |  0.0080000 |    0.17871 |      0.0000 |      4.0000 |
 | "# Phys/StdLooseDplus2KKPi"                     |       500 |       1772 |     3.5440 |     18.410 |      0.0000 |      320.00 |
 | "# Phys/StdLooseDplus2KPiPi"                    |       500 |       1400 |     2.8000 |     14.692 |      0.0000 |      280.00 |
 | "# Phys/StdLooseDplus2KPiPiOppSignPi"           |       500 |       2817 |     5.6340 |     30.756 |      0.0000 |      587.00 |
 | "# Phys/StdLooseDplus2PiPiPi"                   |       500 |       2004 |     4.0080 |     24.352 |      0.0000 |      491.00 |
 | "# Phys/StdLooseDplus2hhh"                      |       500 |      14785 |     29.570 |     113.32 |      0.0000 |      1566.0 |
 | "# Phys/StdLooseDsplus2KKPi"                    |       500 |       1772 |     3.5440 |     18.410 |      0.0000 |      320.00 |
 | "# Phys/StdLooseDsplus2KKPiOppSign"             |       500 |          5 |   0.010000 |    0.14799 |      0.0000 |      3.0000 |
 | "# Phys/StdLooseDstarWithD02KK"                 |       500 |         76 |    0.15200 |    0.76478 |      0.0000 |      9.0000 |
 | "# Phys/StdLooseDstarWithD02KPi"                |       500 |         95 |    0.19000 |    0.82091 |      0.0000 |      12.000 |
 | "# Phys/StdLooseDstarWithD02KPiDCS"             |       500 |        113 |    0.22600 |    0.99545 |      0.0000 |      13.000 |
 | "# Phys/StdLooseDstarWithD02PiPi"               |       500 |        115 |    0.23000 |    0.87926 |      0.0000 |      9.0000 |
 | "# Phys/StdLooseJpsi2MuMu"                      |       500 |         30 |   0.060000 |    0.24576 |      0.0000 |      2.0000 |
 | "# Phys/StdLooseJpsi2ee"                        |       500 |        184 |    0.36800 |     1.0975 |      0.0000 |      9.0000 |
 | "# Phys/StdLooseKsDD"                           |       500 |       2454 |     4.9080 |     6.9446 |      0.0000 |      50.000 |
 | "# Phys/StdLooseKsLD"                           |       500 |       4543 |     9.0860 |     12.906 |      0.0000 |      105.00 |
 | "# Phys/StdLooseKsLD_NegLong"                   |       500 |       2373 |     4.7460 |     7.7241 |      0.0000 |      64.000 |
 | "# Phys/StdLooseKsLD_PosLong"                   |       500 |       2170 |     4.3400 |     6.2907 |      0.0000 |      54.000 |
 | "# Phys/StdLooseKsLL"                           |       500 |        421 |    0.84200 |     1.6640 |      0.0000 |      17.000 |
 | "# Phys/StdLooseKstar2Kpi"                      |       500 |     103638 |     207.28 |     262.54 |      0.0000 |      2112.0 |
 | "# Phys/StdLooseLambdaDD"                       |       500 |       2001 |     4.0020 |     5.4529 |      0.0000 |      36.000 |
""", stdout, result, causes, 
signature_offset = 0, id='Particles')
countErrorLines({"FATAL":0})
</text></argument>
</extension>
