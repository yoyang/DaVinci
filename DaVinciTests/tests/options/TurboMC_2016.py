###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import DaVinci, DecayTreeTuple
from DecayTreeTuple import Configuration
from GaudiConf import IOHelper
from PhysConf.Filters import LoKi_Filters
from TeslaTools import TeslaTruthUtils

hlt2_line = 'Hlt2CharmHadDstp2D0Pip_D02KmPipTurbo'

dtt = DecayTreeTuple('TupleDstToD0pi_D0ToKpi')
dtt.Inputs = ['{0}/Particles'.format(hlt2_line)]
dtt.Decay = '[D*(2010)+ -> ^(D0 -> ^K- ^pi+) ^pi+]CC'
dtt.addBranches({
    'Dst': '[D*(2010)+ -> (D0 -> K- pi+) pi+]CC',
    'D0': '[D*(2010)+ -> ^(D0 -> K- pi+) pi+]CC',
    'D0_K': '[D*(2010)+ -> (D0 -> ^K- pi+) pi+]CC',
    'D0_pi': '[D*(2010)+ -> (D0 -> K- ^pi+) pi+]CC',
    'Dst_pi': '[D*(2010)+ -> (D0 -> K- pi+) ^pi+]CC'
})
dtt.ToolList += [
    'TupleToolMCBackgroundInfo',
    'TupleToolMCTruth'
]
relations = [TeslaTruthUtils.getRelLoc('')]
mc_tools = [
    'MCTupleToolKinematic'
]
TeslaTruthUtils.makeTruth(dtt, relations, mc_tools)

trigger_filter = LoKi_Filters(
    HLT2_Code="HLT_PASS_RE('^{0}Decision$')".format(hlt2_line)
)

DaVinci().EventPreFilters = trigger_filter.filters('TriggerFilters')
DaVinci().UserAlgorithms = [dtt]
DaVinci().TupleFile = 'TurboMC_2016.root'

# Found with `lb-run LHCbDirac dirac-dms-lfn-accessURL --Protocol=xroot /lhcb/MC/2016/DST/00066623/0000/00066623_00000001_1.dst`
IOHelper().inputFiles([
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/MC/2016/DST/00066623/0000/00066623_00000001_1.dst'
])
