#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

"""
Test of the DFTDict module
"""

__author__ = "Alessio PIUCCI alessio.piucci@cern.ch"

from Configurables import DaVinci

from DecayTreeTuple.Configuration import *

from Configurables import FilterDesktop

from PhysSelPython.Wrappers import Selection
from PhysSelPython.Wrappers import SelectionSequence
from PhysSelPython.Wrappers import DataOnDemand

from Configurables import TupleToolDecayTreeFitter, TupleToolDecay, TupleToolKinematic
from Configurables import LoKi__Hybrid__DictOfFunctors, LoKi__Hybrid__Dict2Tuple
from Configurables import LoKi__Hybrid__DTFDict as DTFDict

def get_selection_sequence(name):
    """Get the selection from stripping stream"""
    
    alg = FilterDesktop('SelFilterFor{}B2D'.format(name))
    alg.Code = 'ALL'
    reqSels = [DataOnDemand(Location = 'Phys/Lb2LcDD2HHHPIDBeauty2CharmLine/Particles')]
    sel = Selection('Sel' + name, Algorithm=alg, RequiredSelections=reqSels)
    
    return SelectionSequence('SelSeq' + name, TopSelection=sel)

####################

# some options
DaVinci().Simulation = False
DaVinci().Lumi = True
DaVinci().DataType = '2012'

#for MDST
DaVinci(RootInTES = '/Event/Bhadron', InputType = 'MDST')

DaVinci().EvtMax = 1000
DaVinci().TupleFile = 'test_DFTDict.root'

# define the sequence
seq = GaudiSequencer('MyLb2LcDsDTTupleSeq')

sel = get_selection_sequence('Lb2LcDs')
seq.Members += [sel.sequence()]
tuple_input = sel.outputLocation()

print "sequence test: passed"

# make a tuple
tpl = DecayTreeTuple('Lb2LcDsDTTuple')
tpl.Inputs = [tuple_input]

tpl.ToolList = ['TupleToolKinematic']
tpl.Decay = '[Lambda_b0 -> ^(Lambda_c+ -> ^p+ ^K- ^pi+) ^(D- -> ^K+ ^K- ^pi-)]CC'

# add a branch
tpl.addBranches({"Lb" : "[Lambda_b0 -> Lambda_c+ D-]CC"})

tpl.Lb.ToolList =  ["TupleToolDecayTreeFitter/Cons"] # fit with mass constraint
tpl.Lb.addTool(TupleToolDecayTreeFitter("Cons"))

DictTuple = tpl.Lb.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTuple")

DictTuple.addTool(DTFDict, "Cons")
DictTuple.Source = "LoKi::Hybrid::DTFDict/Cons"
DictTuple.NumVar = 100

DictTuple.Cons.constrainToOriginVertex = True
DictTuple.Cons.daughtersToConstrain = ["Lambda_c+","D_s-"]

DictTuple.Cons.Substitutions = {
    'Lambda_b0 -> Lambda_c+ ^D-' : 'D_s-',
    'Lambda_b~0 -> Lambda_c~- ^D+' : 'D_s+',
    }

print "LoKi__Hybrid__Dict2Tuple test: passed"

# Add LoKiFunctors to the tool chain,
# these functors will be applied to the refitted decay tree
# they act as a source to the DTFDict
DictTuple.Cons.addTool(LoKi__Hybrid__DictOfFunctors,"dict")
DictTuple.Cons.Source = "LoKi::Hybrid::DictOfFunctors/dict"

DictTuple.Cons.dict.Variables = {
    "Cons_PE"                  : "E",
    "Cons_PX"                  : "PX",
    "Cons_PY"                  : "PY",
    "Cons_PZ"                  : "PZ",
    }

print "LoKi__Hybrid__DictOfFunctors test: passed"

# add the tuple to the main sequence
seq.Members += [tpl]

DaVinci().appendToMainSequence([seq])

