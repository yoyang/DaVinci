#!/usr/bin/env gaudirun.py
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# -*- coding: utf-8 -*-
# =============================================================================
# @file
# Test for processing of MC-uDSTs
# @author Vanya BELYAEV Ivan.Belyaev@itep.ru
# @date   2017-05-16
# =============================================================================


from StandardParticles   import StdTightKaons as kaons

from PhysConf.Selections import RebuildSelection
kaons = RebuildSelection ( kaons )

from PhysConf.Selections import CombineSelection
phi   = CombineSelection (
    'Phi' ,  ## the name 
    kaons ,  ## input selection(s)
    ## algorithm properties
    DecayDescriptor = 'phi(1020) -> K+ K-' ,
    CombinationCut  = " AM < 1050*MeV "    ,
    MotherCut       = " ( CHI2VX < 10 ) & ( M < 1045 * MeV )"
    )

from PhysConf.Selections import FilterSelection
phi = FilterSelection (
    "TruePhi" , ## the name 
    phi       , ## input selection(s) 
    ## algorithm properties
    Code      = "mcMatch('phi(1020) => K+ K-', 2 )"   , ## use mcTruth for only long protoparticles
    Preambulo = [ 'from LoKiPhysMC.functions import mcMatch' ] ,
    )

## from PhysConf.Selections import PrintSelection
## phi = PrintSelection ( phi ) 

from PhysConf.Selections import FilterSelection
phi = FilterSelection (
    "MassPhi"  , ## the name 
    phi        , ## input selection(s) 
    ## algorithm properties
    Code      = "0 < monitor( M/GeV , 'MASSPHI', 'MassKK' , histo)"      , 
    Preambulo = [
    'from LoKiCore.functions import monitor,  Gaudi'          ,
    'histo = Gaudi.Histo1DDef( "mass(K+K-)" , 1. , 1.04 , 80 )'
    ] 
    )


from Configurables import DaVinci

daVinci = DaVinci (
    InputType     = 'MDST'                    , 
    DataType      = "2016"                    , 
    Simulation    = True                      ,
    RootInTES     = '/Event/AllStreams'       ,
    ##
    HistogramFile = "DVHistos.root"           ,
    ##
    CondDBtag     = 'sim-20160820-vc-md100'   , 
    DDDBtag       = 'dddb-20150724'           
    )


from PhysConf.Selections import SelectionSequence
seq = SelectionSequence( 'SEQ' , phi ).sequence() 


## assume here the merge request !62 
daVinci.UserAlgorithms = [ phi ]

daVinci.EvtMax = 2000

daVinci.Input  = [
    "DATAFILE='PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/MC/2016/ALLSTREAMS.MDST/00054389/0000/00054389_00000078_3.AllStreams.mdst' SVC='Gaudi::RootEvtSelector' OPT='READ'",
    ]

# =============================================================================
# The END 
# =============================================================================

