###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Valid configuration for running over Turbo data."""
from Configurables import DaVinci

dv = DaVinci()
dv.DataType = '2016'
dv.InputType = 'MDST'
dv.Turbo = True
dv.RootInTES = '/Event/Turbo'
