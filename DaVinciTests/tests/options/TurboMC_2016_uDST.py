###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import DaVinci, DecayTreeTuple, GaudiSequencer
from DecayTreeTuple import Configuration
from GaudiConf import IOHelper
from PhysConf.Filters import LoKi_Filters
from PhysSelPython.Selections import (
    AutomaticData,
    CombineSelection,
    RebuildSelection,
    SelectionSequence
)
from StandardParticles import StdAllLooseANNPions
from TeslaTools import TeslaTruthUtils

hlt2_line = 'Hlt2CharmHadLcpToPpKmPipTurbo'

dtt = DecayTreeTuple('TupleLcTopKpi')
dtt.Inputs = ['{0}/Particles'.format(hlt2_line)]
dtt.Decay = '[Lambda_c+ -> ^p+ ^K- ^pi+]CC'
dtt.addBranches({
    'Lc': '[Lambda_c+ -> p+ K- pi+]CC',
    'Lc_p': '[Lambda_c+ -> ^p+ K- pi+]CC',
    'Lc_K': '[Lambda_c+ -> p+ ^K- pi+]CC',
    'Lc_pi': '[Lambda_c+ -> p+ K- ^pi+]CC'
})
dtt.ToolList += [
    'TupleToolMCBackgroundInfo',
    'TupleToolMCTruth'
]
relations = [
    TeslaTruthUtils.getRelLoc(''),
    # This location is required for truth-matching objects from Turbo++
    '/Event/Turbo/Relations/Hlt2/Protos/Charged'
]
mc_tools = [
    'MCTupleToolKinematic'
]
TeslaTruthUtils.makeTruth(dtt, relations, mc_tools)

# The Lcp line was a PersistReco line in 2016, so we have access to the whole
# HLT2 reconstruction here
combiner_sel = CombineSelection(
    'CombineLcpi',
    inputs=[
        AutomaticData('{0}/Particles'.format(hlt2_line)),
        RebuildSelection(StdAllLooseANNPions)
    ],
    DecayDescriptors=[
        '[Sigma_c++ -> Lambda_c+ pi+]cc',
        '[Sigma_c0 -> Lambda_c+ pi-]cc'
    ],
    DaughtersCuts={
        'pi+': 'PROBNNpi > 0.2'
    },
    CombinationCut=(
        'in_range(0, (AM - AM1 - AM2), 170)'
    ),
    MotherCut=(
        '(VFASPF(VCHI2PDOF) < 10) &'
        'in_range(0, (M - M1 - M2), 150)'
    )
)
selseq = SelectionSequence(
    combiner_sel.name() + 'Sequence',
    TopSelection=combiner_sel
)
sc_dtt = DecayTreeTuple('TupleScToLcpi_LcTopKpi')
sc_dtt.Inputs = [combiner_sel.outputLocation()]
sc_dtt.Decay = '[(Sigma_c++|Sigma_c0) -> ^(Lambda_c+ -> ^p+ ^K- ^pi+) ^X]CC'
sc_dtt.addBranches({
    'Sc': '[Charm -> (Lambda_c+ -> p+ K- pi+) X]CC',
    'Lc': '[Charm -> ^(Lambda_c+ -> p+ K- pi+) X]CC',
    'Lc_p': '[Charm -> (Lambda_c+ -> ^p+ K- pi+) X]CC',
    'Lc_K': '[Charm -> (Lambda_c+ -> p+ ^K- pi+) X]CC',
    'Lc_pi': '[Charm -> (Lambda_c+ -> p+ K- ^pi+) X]CC',
    'Sc_pi': '[Charm -> (Lambda_c+ -> p+ K- pi+) ^X]CC'
})
sc_dtt.ToolList += [
    'TupleToolMCBackgroundInfo',
    'TupleToolMCTruth'
]
TeslaTruthUtils.makeTruth(sc_dtt, relations, mc_tools)
sc_seq = GaudiSequencer('SigmacSequence', Members=[selseq.sequence(), sc_dtt])

trigger_filter = LoKi_Filters(
    HLT2_Code="HLT_PASS_RE('^{0}Decision$')".format(hlt2_line)
)

DaVinci().EventPreFilters = trigger_filter.filters('TriggerFilters')
DaVinci().UserAlgorithms = [dtt, sc_seq]
DaVinci().TupleFile = 'TurboMC_2016_uDST.root'

# Found with `lb-run LHCbDirac dirac-dms-lfn-accessURL --Protocol=xroot /lhcb/MC/2016/ALLSTREAMS.MDST/00063447/0000/00063447_00000001_7.AllStreams.mdst`
IOHelper().inputFiles([
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/MC/2016/ALLSTREAMS.MDST/00063447/0000/00063447_00000001_7.AllStreams.mdst'
])
