###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##############################################################################
# $Id: DVTestCommonParticles.py,v 1.3 2010/01/06 08:51:50 pkoppenb Exp $
#
# syntax: gaudirun.py $DAVINCIMONITORSROOT/options/DVMonitorDst.py
#
# Author: Patrick Koppenburg <patrick.koppenburg@cern.ch>
#
##############################################################################
from DaVinci.Configuration import DaVinci
from Gaudi.Configuration import *
##############################################################################
#
from CommonParticles import StandardBasic
from CommonParticles import StandardIntermediate

Locations = []
for a,b in StandardIntermediate.locations.iteritems():
    print "DVTestCommonParticles adding location", a
    Locations.append(a)

from Configurables import CountParticles
CP = CountParticles(Inputs = Locations)
##############################################################################
#
# Histograms
#
DaVinci().HistogramFile = "DVStandardIntermediate.root"
##############################################################################
#
# Most of this will be configured from Dirac
#
##############################################################################
DaVinci().UserAlgorithms = [ CP ]   # count them all
DaVinci().EvtMax = 500
DaVinci().DataType = "2015"
DaVinci().Simulation = False
DaVinci().InputType = "DST"

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
