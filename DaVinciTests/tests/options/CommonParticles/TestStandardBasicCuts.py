###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test validisy of CommonParticles.StandardBasic cuts.

   syntax: gaudirun.py $DAVINCIMONITORSROOT/options/TestStandardBasicCuts.py

   Author: Juan Palacios <palacios@physik.uzh.ch>

"""
##############################################################################
from DaVinci.Configuration import DaVinci
from Gaudi.Configuration import *
#
from CommonParticles import StandardBasic

List = []

for a,b in StandardBasic.locations.iteritems():
    print "DVTestCommonParticles adding location", a
    List.append(b)                      # ist of algorithms

DaVinci( UserAlgorithms = List,
         EvtMax = 1,
         DataType = "2010",
         Simulation = False,
         InputType = "DST"       )

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
