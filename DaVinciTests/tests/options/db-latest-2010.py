###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from Configurables import LHCbApp, CondDB, DDDBConf

dataType="2010"

DDDBConf(DataType=dataType)
CondDB(IgnoreHeartBeat=True)

if "UseLatestTags" in CondDB().__slots__:
    CondDB().UseLatestTags=[dataType]
else:
    CondDB().useLatestTags(DataType=dataType, OnlyGlobalTags=False)
