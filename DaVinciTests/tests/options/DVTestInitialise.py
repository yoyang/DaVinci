###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## $Id: DVTestInitialise.py,v 1.4 2010/02/23 10:33:21 jpalac Exp $
## ============================================================================
## CVS tag $Name:  $
## ============================================================================
"""
@file $Name:  $

Test for DaVinci initialisation

@author P. Koppenburg
@date 2009-01-07
"""
from Gaudi.Configuration import *
from Configurables import DaVinci
DaVinci().MainOptions = ""
DaVinci().UserAlgorithms = [ ]

DaVinci().EvtMax = 0 # No running 
DaVinci().SkipEvents = 0 
DaVinci().PrintFreq = 1 
DaVinci().DataType = '2009'
DaVinci().Lumi = True
########################################################################
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

