###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

########################################################################
from Gaudi.Configuration import *
from Configurables import GaudiSequencer
from Configurables import DaVinci, PhysConf
########################################################################

DaVinci().EvtMax     = 1000                 # Number of events
DaVinci().PrintFreq  = 100                  # Events to skip
DaVinci().DataType   = "2012"               # Must be given
DaVinci().HistogramFile = "udst-unpacking.root"
DaVinci().InputType  = "MDST"

########################################################################

from Configurables import DataObjectVersionFilter
DaVinci().UserAlgorithms += [
    DataObjectVersionFilter( "CheckDecReports",
                             DataObjectLocation = "/Event/Strip/Phys/DecReports",
                             MaxVersion = 999,
                             OutputLevel = 3 ),
    DataObjectVersionFilter( "CheckTracks",
                             DataObjectLocation = "/Event/Bhadron/Rec/Track/Best",
                             MaxVersion = 999 ),
    DataObjectVersionFilter( "CheckVeloClusters",
                             DataObjectLocation = "/Event/Bhadron/Raw/Velo/Clusters",
                             MaxVersion = 999,
                             OutputLevel = 3 ),
    DataObjectVersionFilter( "CheckCaloElectrons",
                             DataObjectLocation = "/Event/Bhadron/Rec/Calo/Electrons",
                             MaxVersion = 999,
                             OutputLevel = 3 ),
    DataObjectVersionFilter( "CheckCaloPhotons",
                             DataObjectLocation = "/Event/Bhadron/Rec/Calo/Photons",
                             MaxVersion = 999,
                             OutputLevel = 3 ),
#    DataObjectVersionFilter( "CheckLiteTTClusters",
#                             DataObjectLocation = "/Event/Bhadron/Raw/TT/LiteClusters",
#                             MaxVersion = 999,
#                             OutputLevel = 3 ),
#    DataObjectVersionFilter( "CheckLiteITClusters",
#                             DataObjectLocation = "/Event/Bhadron/Raw/IT/LiteClusters",
#                             MaxVersion = 999,
#                             OutputLevel = 3 ),
#    DataObjectVersionFilter( "CheckLiteVeloClusters",
#                             DataObjectLocation = "/Event/Bhadron/Raw/Velo/LiteClusters",
#                             MaxVersion = 999,
#                             OutputLevel = 3 ),
    DataObjectVersionFilter( "CheckBhadronKaons",
                             DataObjectLocation = "/Event/Bhadron/Phys/StdAllNoPIDsKaons/Particles",
                             MaxVersion = 999,
                             OutputLevel = 3 ),
    DataObjectVersionFilter( "CheckPVs",
                             DataObjectLocation = "/Event/Bhadron/Rec/Vertex/Primary",
                             MaxVersion = 999,
                             OutputLevel = 3 )
    ]
