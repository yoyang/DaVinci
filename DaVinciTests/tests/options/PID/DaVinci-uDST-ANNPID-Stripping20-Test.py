###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

########################################################################
from Gaudi.Configuration import *
from Configurables import GaudiSequencer
from Configurables import DaVinci, PhysConf
########################################################################

DaVinci().EvtMax     = 1000                  # Number of events
DaVinci().PrintFreq  = 100                   # Events to skip
DaVinci().DataType   = "2012"                # Must be given
DaVinci().HistogramFile = "udst-annpid.root"
DaVinci().InputType  = "MDST"

########################################################################

from Configurables import ( ANNGlobalPID__ChargedProtoANNPIDMoni,
                            TESCheck, GaudiSequencer )
protoLoc = "/Event/Bhadron/Rec/ProtoP/Charged"
seq = GaudiSequencer("ANNPIDCheckSeq")
seq.Members += [
    TESCheck( "CheckProtos",
              Inputs = [protoLoc],
              OutputLevel = 5,
              Stop = False ),
    ANNGlobalPID__ChargedProtoANNPIDMoni( "ANNPIDMoni",
                                          ProtoParticleLocation = protoLoc )
    ]
DaVinci().UserAlgorithms = [seq]
