###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *

from Configurables import GaudiSequencer
seqGenFSR = GaudiSequencer("GenFSRSeq")
seqGenFSR.Members += [ "GenFSRMerge" ]
seqGenFSR.Members += [ "GenFSRLog" ]

from Configurables import LHCbApp
LHCbApp().DDDBtag   = "dddb-20130929-1"
LHCbApp().CondDBtag = "sim-20130522-1-vc-md100"

from Configurables import DaVinci
DaVinci().InputType = "DST"
DaVinci().DataType  = "2012"
DaVinci().Simulation   = True
DaVinci().UserAlgorithms = [seqGenFSR]

from PRConfig import TestFileDB
TestFileDB.test_file_db["genFSR_2012_dst"].run(configurable=DaVinci())
