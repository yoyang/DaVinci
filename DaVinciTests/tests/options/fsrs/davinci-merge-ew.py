###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
importOptions('$APPCONFIGOPTS/Merging/DV-Stripping-Merging.py')
importOptions('$APPCONFIGOPTS/Merging/WriteFSR.py')
importOptions('$DAVINCITESTSROOT/tests/options/fsrs/davinci-merge.py')
EventSelector().Input = ["DATAFILE='PFN:MagUp.EW.dst' TYP='POOL_ROOTTREE' OPT='READ'", "DATAFILE='PFN:MagDown.EW.dst' TYP='POOL_ROOTTREE' OPT='READ'"]
