###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Example Options for BTagging algorithm

@author Marco Musy, Kevin Heinicke
@date 2010-09-29, 2018-05-03
"""

from Gaudi.Configuration import *
import GaudiPython
from Configurables import GaudiSequencer

# some selections

from Configurables import CheatedSelection, BDecayTool, MCDecayFinder

from GaudiKernel import ROOT6WorkAroundEnabled
if ROOT6WorkAroundEnabled('ROOT-7492'):
    # trigger autoloading of LHCbKernel dictionary
    GaudiPython.gbl.LHCb.LHCbID
    # trigger autoloading of DigiEvent dictionary
    GaudiPython.gbl.LHCb.CaloDigit

myDecay = '[B+ -> (J/psi(1S) => mu+ mu- ) K+]cc'

cheatsel = CheatedSelection("CheatedSelection")
cheatsel.Inputs = [ "Phys/TaggingPions" ]
cheatsel.AssociatorInputData = [ "Phys/CheatedSelection/Particles" ]
cheatsel.OutputLevel = 5

cheatsel.addTool( BDecayTool )
cheatsel.BDecayTool.addTool( MCDecayFinder )
cheatsel.BDecayTool.MCDecayFinder.Decay = myDecay


# setup tagging

location = "Phys/CheatedSelection"

from Configurables import BTagging, BTaggingTool, BTaggingChecker
from FlavourTagging.Tunings import applyTuning

tag = BTagging("BTagging")
tag.Inputs = [ location ]
#tag.OutputLevel = 5
tag.addTool( BDecayTool )
tag.BDecayTool.addTool( MCDecayFinder )
tag.BDecayTool.MCDecayFinder.Decay = myDecay
tag.addTool( BTaggingTool )
applyTuning(tag.BTaggingTool, 'TestTuning')
tag.BTaggingTool.AssocPVCriterion = 'PVbyIP'  # needed by CheatedSelection

# Flavour tagging Checker
tagcheck = BTaggingChecker("BTaggingChecker")
tagcheck.Inputs = [ location ]
tagcheck.TagsLocation = location+"/FlavourTags"
# tagcheck.OutputLevel = DEBUG
tagcheck.addTool( BDecayTool )
tagcheck.BDecayTool.addTool( MCDecayFinder )
tagcheck.BDecayTool.MCDecayFinder.Decay = myDecay
MessageSvc().Format = "% F%40W%S%7W%R%T %0W%M"


# standard configuration

from Configurables import DaVinci
DaVinci().EvtMax     = 1000                       # Number of events
DaVinci().SkipEvents = 0                          # Events to skip
DaVinci().PrintFreq  = 100
DaVinci().DataType = "2012"
DaVinci().CondDBtag = 'sim-20121025-vc-mu100'
DaVinci().DDDBtag = "dddb-20120831"
DaVinci().Simulation    = True
from Configurables import StoreExplorerAlg, PrintDecayTree
PrintDecayTree().Inputs = [ location ]
DaVinci().MoniSequence = [
        cheatsel,
        tag,
        tagcheck,
        ]  # The algorithms

#from GaudiConf import IOHelper
#IOHelper().inputFiles(['PFN:root://eoslhcb.cern.ch//eos/lhcb/freezer/lhcb/MC/2012/ALLSTREAMS.DST/00021062/0000/00021062_00000001_1.allstreams.dst']) # Bu2JpsiK

importOptions( '$PRCONFIGOPTS/DaVinci/FlavourTagging/MC12-B2JPsiMuMuK.py' )

appMgr = GaudiPython.AppMgr()

appMgr.run( DaVinci().EvtMax )
