###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Valid configuration for running over Turbo data.

We don't set InputType to MDST explicitly, but this should be done by the
DaVinci configuration.
"""
from Configurables import DaVinci

dv = DaVinci()
dv.DataType = '2016'
# This value should be set automatically as Turbo = True (although it's better
# to set it explicitly anyway)
# dv.InputType = 'MDST'
dv.Turbo = True
dv.RootInTES = '/Event/Turbo'
