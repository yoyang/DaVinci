###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Check ntuples made from Turbo MC input for correct truth-matching."""
from __future__ import print_function
import sys

import ROOT

tree_branches = {
    'TupleDstToD0pi_D0ToKpi': (['Dst', 'D0'], ['D0_K', 'D0_pi', 'Dst_pi']),
    'TupleLcTopKpi': (['Lc'], ['Lc_p', 'Lc_K', 'Lc_pi']),
    'TupleScToLcpi_LcTopKpi': (['Sc', 'Lc'], ['Lc_p', 'Lc_K', 'Lc_pi', 'Sc_pi'])
}

fname = sys.argv[-2]
tname = sys.argv[-1]
assert fname.endswith('.root'), 'Expected a ROOT file as input, got: ' + fname
assert tname in tree_branches, 'Cannot handle given tree, got: ' + tname
print('Looking at file {0!r}, tree {1!r}'.format(fname, tname))

composites, children = tree_branches[tname]

f = ROOT.TFile(fname)
t = f.Get('{0}/DecayTree'.format(tname))

assert t.GetEntries() > 0, 'Expected non-zero number of entries'
print('Found {0} entries'.format(t.GetEntries()))

# If the truth-matching fails, we'd see BKGCAT == 60 for all entries
for p in composites:
    condition = '{0}_BKGCAT < 20'.format(p)
    assert t.GetEntries(condition) > 0, \
           'Expected non-zero number of entries with {0}'.format(condition)
    print('OK: {0!r}'.format(condition))
# If the truth-matching fails, we'd see TRUEID == 0 for all entries
for p in composites + children:
    condition = '{0}_TRUEID != 0'.format(p)
    assert t.GetEntries(condition) > 0, \
           'Expected non-zero number of entries with {0}'.format(condition)
    print('OK: {0!r}'.format(condition))
f.Close()
