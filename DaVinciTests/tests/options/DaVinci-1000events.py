###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci

# Number of events etc.
DaVinci().EvtMax    = 1000
DaVinci().PrintFreq = 10

#DaVinci().SkipEvents = 17100

# Temporary. Force a particular CondDB tag
#from Configurables import LHCbApp
#LHCbApp().DDDBtag="head-20120413"
#LHCbApp().CondDBtag="cond-20131028"
